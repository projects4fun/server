#### About this repository

This repository is a multi module maven project providing items that could be
found in several application servers. The module `server-commons` provides all
modules in this project as well as some additional packages such as logging 
facades, common text operation dependencies, etc.

The intention of the project is to provide a one stop dependency to providing
most things I have found to be essential when doing development on top of the
JVM and your target is not a application server such as Tomcat (EE) / Wildfly.
This project is more akin to dropwizard over traditional application servers.

A second intention is to be able to have all other modules as stand alone 
components. All modules support a programmatic configuration if desired / if the
configuration file does not provide adequate coverage.