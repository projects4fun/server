package com.projects4fun.server.sql.jpa;

import com.projects4fun.server.sql.configuration.PersistenceConfigurationImpl;
import com.projects4fun.server.sql.model.ApplicationUser;
import com.projects4fun.server.sql.query.QueryBuilder;
import org.junit.jupiter.api.*;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * @author Juan Garcia
 * @since 2017-11-27
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PersistenceTransactionTest {

    @BeforeAll
    static void setup() {
        PersistenceManager.initEntityFactory(
                new PersistenceConfigurationImpl(PersistenceTransactionTest.class.getName())
        );
    }

    @Test
    @DisplayName("PersistenceTransaction does allow for an object to be saved")
    void persistence_transaction_does_save_object() {
        try (PersistenceTransaction transaction = new PersistenceTransaction()) {
            EntityManager entityManager = transaction.getEntityManager();

            ApplicationUser applicationUser = new ApplicationUser();
            applicationUser.setFirstName("Transaction");
            applicationUser.setLastName("Test");

            entityManager.persist(applicationUser);
        }
    }

    @Test
    @DisplayName("Previously saved object can be identified")
    void previously_saved_object_can_be_identified() {
        try (PersistenceInstance instance = new PersistenceInstance()) {
            List<ApplicationUser> users =
                    new QueryBuilder<>(ApplicationUser.class, instance)
                            .eq("firstName", "Transaction")
                            .get();

            Assertions.assertTrue(users.size() == 1);
        }
    }

    @AfterAll
    static void cleanup() {
        PersistenceManager.closeFactory(PersistenceConfiguration.DEFAULT_CONNECTION_NAME);
    }
}
