package com.projects4fun.server.sql.jpa;

import com.projects4fun.server.sql.configuration.PersistenceConfigurationImpl;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * @author Juan Garcia
 * @since 2017-11-26
 */
class PersistenceManagerTest {

    @Test
    @DisplayName("EntityManagerFactory can be be closed")
    void entity_manager_factory_can_be_closed() {
        PersistenceManager.initEntityFactory(
                new PersistenceConfigurationImpl(this.getClass().getName())
        );

        PersistenceManager.closeFactory(PersistenceConfiguration.DEFAULT_CONNECTION_NAME);
    }
}
