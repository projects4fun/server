package com.projects4fun.server.sql;

import com.projects4fun.server.sql.configuration.PersistenceConfigurationImpl;
import com.projects4fun.server.sql.model.ApplicationUser;
import com.projects4fun.server.sql.model.dao.ApplicationUserDAO;
import com.projects4fun.server.sql.jpa.PersistenceManager;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Optional;

/**
 * <p></p>
 *
 * @author Juan Garcia
 * @since 2018-07-23
 */
class DataAccessObjectTest {

    private static ApplicationUserDAO userDAO;

    @BeforeAll
    static void setup() {
        PersistenceManager.initEntityFactory(
                new PersistenceConfigurationImpl(DataAccessObjectTest.class.getName())
        );
        userDAO = new ApplicationUserDAO();

    }

    @Test
    void object_can_be_persisted_to_database() {
        ApplicationUser user = new ApplicationUser();
        user.setFirstName("demo first name");

        user = userDAO.create(user);

        Assertions.assertNotNull(user);
        Assertions.assertEquals("demo first name", user.getFirstName());
        Assertions.assertFalse(userDAO.list().isEmpty());
    }

    @Test
    void object_that_has_been_persisted_can_be_updated() {
        ApplicationUser user = new ApplicationUser();
        user.setFirstName("first name");

        user = userDAO.create(user);

        Assertions.assertNotNull(user);
        Assertions.assertEquals("first name", user.getFirstName());

        user.setFirstName("revised first name");
        user = userDAO.update(user);

        long userId = user.getId();
        Assertions.assertEquals(userId, user.getId());
        Assertions.assertEquals("revised first name", user.getFirstName());
        Assertions.assertFalse(userDAO.list().isEmpty());
    }

    @Test
    void object_that_has_been_persisted_can_be_removed() {
        ApplicationUser user = new ApplicationUser();
        user.setFirstName("to be deleted");

        user = userDAO.create(user);

        Assertions.assertNotNull(user);
        Assertions.assertEquals("to be deleted", user.getFirstName());

        user = userDAO.remove(user);
        Optional<ApplicationUser> optional = userDAO.findById(user.getId());
        Assertions.assertFalse(optional.isPresent());
    }
}
