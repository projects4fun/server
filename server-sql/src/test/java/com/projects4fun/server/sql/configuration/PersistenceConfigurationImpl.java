package com.projects4fun.server.sql.configuration;

import com.projects4fun.server.sql.jpa.PersistenceConfiguration;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Juan Garcia
 * @since 2017-11-26
 */
public class PersistenceConfigurationImpl implements PersistenceConfiguration {

    final private String className;

    public PersistenceConfigurationImpl(final String className) {
        this.className = className;
    }

    @Override
    public String entityFactoryName() {
        return DEFAULT_CONNECTION_NAME;
    }

    @Override
    public String getPersistenceUnitName() {
        return "JUNIT_TESTS";
    }

    @Override
    public Map<String, String> getProperties() {
        Map<String, String> properties = new HashMap<>();

        properties.put("javax.persistence.jdbc.url", "jdbc:h2:./target/" + className);
        properties.put("javax.persistence.jdbc.user", "sa");
        properties.put("javax.persistence.jdbc.password", "sa");
        properties.put("javax.persistence.jdbc.driver", "org.h2.Driver");
        properties.put("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
        properties.put("hibernate.show_sql", "true");

        return properties;
    }
}
