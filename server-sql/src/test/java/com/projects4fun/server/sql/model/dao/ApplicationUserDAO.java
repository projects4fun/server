package com.projects4fun.server.sql.model.dao;

import com.projects4fun.server.sql.DataAccessObject;
import com.projects4fun.server.sql.model.ApplicationUser;

/**
 * <p></p>
 *
 * @author Juan Garcia
 * @since 2018-07-23
 */
public class ApplicationUserDAO extends DataAccessObject<ApplicationUser, Long> {
}
