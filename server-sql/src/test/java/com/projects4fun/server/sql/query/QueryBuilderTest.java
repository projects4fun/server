package com.projects4fun.server.sql.query;

import com.projects4fun.server.sql.configuration.PersistenceConfigurationImpl;
import com.projects4fun.server.sql.jdbc.ConnectionPool;
import com.projects4fun.server.sql.jpa.PersistenceConfiguration;
import com.projects4fun.server.sql.jpa.PersistenceInstance;
import com.projects4fun.server.sql.jpa.PersistenceManager;
import com.projects4fun.server.sql.model.ApplicationUser;
import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Juan Garcia
 * @since 2017-11-26
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class QueryBuilderTest {

    @BeforeAll
    void setup() {
        ConnectionPool.createPool("jdbc:h2:./target/" + QueryBuilderTest.class.getName(),
                "sa", "sa");
        PersistenceManager.initEntityFactory(
                new PersistenceConfigurationImpl(QueryBuilderTest.class.getName())
        );
    }

    @Test
    @DisplayName("QueryBuilder for equality returns a specific value")
    void query_builder_equality_returns_specific_value() {
        try (PersistenceInstance persistenceInstance = new PersistenceInstance()) {
            List<ApplicationUser> users =
                    new QueryBuilder<>(ApplicationUser.class, persistenceInstance)
                            .eq("firstName", "DEMO")
                            .get();

            Assertions.assertEquals(1, users.size());
            Assertions.assertEquals("DEMO", users.get(0).getFirstName());
        }
    }

    @Test
    @DisplayName("QueryBuilder for equality returns a specific value")
    void query_builder_with_resources_equality_returns_specific_value() {
        try (QueryBuilder<ApplicationUser> builder = new QueryBuilder<>(ApplicationUser.class)) {
            List<ApplicationUser> users =
                builder.eq("firstName", "DEMO")
                    .get();

            Assertions.assertEquals(1, users.size());
            Assertions.assertEquals("DEMO", users.get(0).getFirstName());
        }
    }

    @Test
    @DisplayName("QueryBuilder for equality expression in same column returns no results")
    void query_builder_equality_expression_same_column_returns_zero_results() {
        try (PersistenceInstance persistenceInstance = new PersistenceInstance()) {
            List<ApplicationUser> users =
                    new QueryBuilder<>(ApplicationUser.class, persistenceInstance)
                            .eq("firstName", "DEMO")
                            .eq("firstName", "DEMO_TWO")
                            .get();

            Assertions.assertEquals(0, users.size());
        }
    }

    @Test
    @DisplayName("QueryBuilder for non equality expression omits results")
    void query_builder_non_equality_omits_results() {
        try (PersistenceInstance i = new PersistenceInstance()) {
            List<ApplicationUser> users = new QueryBuilder<>(ApplicationUser.class, i)
                .notEq("firstName", "DEMO")
                .get();

            for (ApplicationUser user : users) {
                Assertions.assertNotEquals("DEMO", user.getFirstName());
            }
        }
    }

    @Test
    @DisplayName("QueryBuilder with like expression provides all matching results")
    void query_builder_like_returns_matching_results() {
        final String pattern = "DEMO";
        try (PersistenceInstance persistenceInstance = new PersistenceInstance()) {
            List<ApplicationUser> users =
                    new QueryBuilder<>(ApplicationUser.class, persistenceInstance)
                            .like("firstName", pattern + "%")
                            .get();


            Assertions.assertEquals(2, users.size());
            for (ApplicationUser user : users) {
                Assertions.assertTrue(user.getFirstName().startsWith(pattern));
            }
        }
    }

    @Test
    @DisplayName("QueryBuilder for or expression returns two results")
    void query_builder_for_or_returns_two_results() {
        try (PersistenceInstance persistenceInstance = new PersistenceInstance()) {
            List<ApplicationUser> users = new QueryBuilder<>(ApplicationUser.class, persistenceInstance)
                    .or(new QueryCriteria[]{new QueryCriteria<>("firstName", "DEMO"),
                            new QueryCriteria<>("firstName", "DEMO_TWO")}).get();

            Assertions.assertEquals(2, users.size());
        }
    }

    @Test
    @DisplayName("QueryBuilder does allow an ArrayList")
    void query_builder_does_allow_array_list() {
        try (PersistenceInstance persistenceInstance = new PersistenceInstance()) {
            List<QueryCriteria> queryCriteria = new ArrayList<>();
            queryCriteria.add(new QueryCriteria<>("firstName", "DEMO"));
            queryCriteria.add(new QueryCriteria<>("firstName", "DEMO_TWO"));

            List<ApplicationUser> users = new QueryBuilder<>(ApplicationUser.class, persistenceInstance)
                    .or(queryCriteria)
                    .get();

            Assertions.assertEquals(2, users.size());
        }
    }

    @Test
    @DisplayName("QueryBuilder that uses limit returns only declared max results")
    void query_builder_with_limit_returns_limit_value() {
        try (PersistenceInstance persistenceInstance = new PersistenceInstance()) {
            List<ApplicationUser> users = new QueryBuilder<>(ApplicationUser.class, persistenceInstance)
                    .limit(1)
                    .get();

            Assertions.assertEquals(1, users.size());
        }
    }

    @Test
    @DisplayName("QueryBuilder isNull returns values where the property is null")
    void query_builder_is_null_returns_values_with_is_null_property() {
        try (PersistenceInstance persistenceInstance = new PersistenceInstance()) {
            List<ApplicationUser> users =
                    new QueryBuilder<>(ApplicationUser.class, persistenceInstance)
                            .isNull("middleName")
                            .get();

            for (ApplicationUser user : users) {
                Assertions.assertNull(user.getMiddleName());
            }
        }
    }

    @Test
    @DisplayName("QueryBuilder isNotNull returns values where the property is not null")
    void query_builder_is_not_null_returns_values_with_is_not_null_property() {
        try (PersistenceInstance persistenceInstance = new PersistenceInstance()) {
            List<ApplicationUser> users =
                    new QueryBuilder<>(ApplicationUser.class, persistenceInstance)
                            .isNotNull("middleName")
                            .get();

            for (ApplicationUser user : users) {
                Assertions.assertNotNull(user.getMiddleName());
            }
        }
    }

    @AfterAll
    void cleanup() {
        PersistenceManager.closeFactory(PersistenceConfiguration.DEFAULT_CONNECTION_NAME);
    }
}
