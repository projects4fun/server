package com.projects4fun.server.sql.jdbc;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author Juan Garcia
 * @since 2017-11-26
 */
class ConnectionPoolTest {

    @BeforeAll
    static void setup() throws Exception {
        Files.deleteIfExists(new File("target/" +
                ConnectionPoolTest.class.getSimpleName() + ".mv.db").toPath());
        ConnectionPool.createPool("jdbc:h2:./target/" +
                ConnectionPoolTest.class.getSimpleName(), "sa", "sa");
    }

    @Test
    @DisplayName("A created ConnectionPool can use a Connection to perform a SQL query")
    void obtained_connection_can_be_used_to_perform_sql_query() {
        try (Connection connection = ConnectionPool.getConnection()) {
            try (Statement statement = connection.createStatement()) {
                try (ResultSet resultSet = statement.executeQuery("SELECT 1 FROM DUAL")) {
                    resultSet.next();

                    Assertions.assertTrue(resultSet.getInt(1) == 1);
                }
            }
        } catch (Exception e) {
            Assertions.fail(e);
        }
    }

    @Test
    @DisplayName("An established ConnectionPool with invalid name throws IllegalArgumentException")
    void invalid_name_throws_illegal_argument_exception() {
        Throwable exception = assertThrows(IllegalArgumentException.class,
                () -> ConnectionPool.getConnection("INVALID_NAME"));
    }

    @Test
    @DisplayName("Established ConnectionPool can be shutdown")
    void established_connection_pool_can_be_shutdown() {
        ConnectionPool.createPool("jdbc:h2:./target/one_" +
                        ConnectionPoolTest.class.getSimpleName(), "sa",
                "sa", "JUNIT_SOURCE_ID");
        ConnectionPool.close("JUNIT_SOURCE_ID");
    }
}
