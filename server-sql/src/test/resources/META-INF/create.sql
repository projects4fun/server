-- IMPORTANT.
-- A create statement requires that the create be in one line.
CREATE TABLE IF NOT EXISTS application_user (id IDENTITY PRIMARY KEY, first_name VARCHAR(300), middle_name VARCHAR(300), last_name VARCHAR(300));
COMMIT;