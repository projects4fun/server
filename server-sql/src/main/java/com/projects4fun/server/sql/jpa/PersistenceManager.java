package com.projects4fun.server.sql.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * <p>Permits the configuration of a {@link EntityManagerFactory} and provides a caching mechanism
 * for multiple {@link EntityManagerFactory} instances. The multiple instances support is intended
 * to support multiple database back ends permitting access to a {@link EntityManager} by calling
 * the named {@link #getEntityManager(String)}.</p>
 *
 * @author Juan Garcia
 * @since 2017-11-25
 */
public class PersistenceManager {

    final private static Logger logger = Logger.getLogger(PersistenceManager.class.getName());

    private static Map<String, EntityManagerFactory> entityManagerFactoryMap = new HashMap<>();

    /**
     * <p>Initializes a {@link EntityManagerFactory} with a {@link PersistenceConfiguration}. When
     * used in an environment where many backend databases are supported {@link EntityManagerFactory}
     * should be uniquely named via {@link PersistenceConfiguration#entityFactoryName()}.</p>
     *
     * @param configuration details to help register a {@link EntityManagerFactory}
     */
    public static void initEntityFactory(final PersistenceConfiguration configuration) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory(
            configuration.getPersistenceUnitName(), configuration.getProperties());

        entityManagerFactoryMap.put(configuration.entityFactoryName(), entityManagerFactory);
    }

    /**
     * <p>Permits the singular closing of a {@link EntityManagerFactory}. This should be called in a
     * multi tenant environment where one client needs to have its database connections closed
     * down.</p>
     *
     * @param entityFactoryName the name of the {@link EntityManagerFactory} to shutdown
     */
    public static synchronized void closeFactory(final String entityFactoryName) {
        if (entityFactoryName == null) {
            throw new IllegalArgumentException("A null entity factory name is not valid.");
        }

        logger.finest(() -> String.format("Searching factory entry map for: %s", entityFactoryName));
        if (entityManagerFactoryMap.containsKey(entityFactoryName)) {
            entityManagerFactoryMap.get(entityFactoryName).close();
            entityManagerFactoryMap.remove(entityFactoryName);
        } else {
            throw new IllegalArgumentException("Provided entity factory was not found.");
        }
    }

    /**
     * <p>Attempts to close all instances {@link EntityManagerFactory} that may have been created
     * throughout the applications life.</p>
     */
    public static synchronized void closeFactories() {
        for (Map.Entry<String, EntityManagerFactory> entry : entityManagerFactoryMap.entrySet()) {
            entry.getValue().close();
        }

        entityManagerFactoryMap = new HashMap<>();
    }

    /**
     * <p>Obtains the default {@link EntityManager}. This {@link EntityManager} should be released
     * when consumption is complete. Failure to do so will end the amount of connections that can be
     * made to the database.</p>
     *
     * @return an instance of {@link EntityManager} with the default pool name
     */
    public static EntityManager getEntityManager() {
        return getEntityManager(PersistenceConfiguration.DEFAULT_CONNECTION_NAME);
    }

    /**
     * <p>See {@link #getEntityManager()}. The {@link EntityManager} is a named instance of
     * {@link EntityManagerFactory}.</p>
     *
     * @param entityFactoryName the unique name to identify the {@link EntityManagerFactory}
     * @return an instance of {@link EntityManager} associated to the {@link String} argument
     */
    public static EntityManager getEntityManager(final String entityFactoryName) {
        if (entityFactoryName == null) {
            throw new IllegalArgumentException("A null entity factory name is not valid.");
        }

        if (entityManagerFactoryMap.containsKey(entityFactoryName)) {
            return entityManagerFactoryMap.get(entityFactoryName).createEntityManager();
        } else {
            throw new IllegalArgumentException("An entity with the name " + entityFactoryName
                + " was not found.");
        }
    }
}
