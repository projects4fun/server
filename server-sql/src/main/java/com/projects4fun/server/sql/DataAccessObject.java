package com.projects4fun.server.sql;

import com.projects4fun.server.sql.jpa.PersistenceInstance;
import com.projects4fun.server.sql.jpa.PersistenceTransaction;
import com.projects4fun.server.sql.query.QueryBuilder;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Optional;

/**
 * <p>Declares the methods that should be implemented by all models that connect
 * to a database. The interface aligns with CRUD functionality leaving more
 * advanced items to be performed by SQL or {@link QueryBuilder}.</p>
 *
 * <p>{@link #list()} and {@link #findById(Object)} leverage reflection to
 * obtain the type of object to be returned.</p>
 *
 * @author Juan Garcia
 * @since 2017-11-26
 */
public abstract class DataAccessObject<E, K> implements Serializable {

    final private Class<E> type;

    @SuppressWarnings("unchecked")
    public DataAccessObject() {
        this.type = ((Class<E>) ((ParameterizedType) getClass()
            .getGenericSuperclass()).getActualTypeArguments()[0]);
    }

    /**
     * <p>Provide all instances of type {@link E}. Care should be provided for
     * types that may return a large volume of content such as a list of all
     * transactions, accounts, people, etc.</p>
     *
     * @return a {@link List} of type {@link E}
     */
    public List<E> list() {
        try (PersistenceInstance i = new PersistenceInstance()) {
            QueryBuilder<E> builder = new QueryBuilder<>(type, i);
            return builder.get();
        }
    }

    /**
     * <p>Declares that an entity should be created. The implementor should rely
     * on the this keyword to instruct which object is to be created.</p>
     *
     * @return an object of type E
     */
    public E create(E object) {
        try (PersistenceTransaction t = new PersistenceTransaction()) {
            t.getEntityManager().persist(object);
            return object;
        }
    }

    /**
     * <p>Declares that an entity should be updated. The implementor should rely
     * on the this keyword to instruct which object is to be removed.</p>
     *
     * @return an object of type E
     */
    public E update(E object) {
        try (PersistenceTransaction t = new PersistenceTransaction()) {
            t.getEntityManager().merge(object);
            return object;
        }
    }

    /**
     * <p>Declares that an entity should be removed. The implementor should rely
     * on the this keyword to instruct which object is to be removed.</p>
     *
     * @return an object of type E
     */
    public E remove(E object) {
        try (PersistenceTransaction t = new PersistenceTransaction()) {
            t.getEntityManager().remove(t.getEntityManager().merge(object));
            return object;
        }
    }

    /**
     * <p>Declares that an entity should be searched for on its primary key
     * value.</p>
     *
     * @param id the value that can be used to identify this item in a database.
     * @return an object of type E
     */
    public Optional<E> findById(K id) {
        try (PersistenceInstance i = new PersistenceInstance()) {
            E o = i.getEntityManager().find(type, id);
            return o == null ? Optional.empty() : Optional.of(o);
        }
    }
}
