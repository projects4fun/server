package com.projects4fun.server.sql.query;

import com.projects4fun.server.sql.jpa.PersistenceInstance;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * <p>A wrapper around the JPA {@link CriteriaBuilder} and {@link TypedQuery}.
 * The wrapper is to permit an easier means of performing dynamic at runtime
 * defined queries such as those that may be created from a UI while still
 * leveraging JPA to perform the queries.</p>
 *
 * <p>There is a order of operations! Predicate builders {@link #eq(String, Object)},
 * {@link #isNull(String)} come before {@link TypedQuery} qualifiers {@link #limit(int)}
 * finally executing the query is last {@link #get()}. Doing these actions out
 * of order is likely to cause unexpected results.</p>
 *
 * @author Juan Garcia
 * @since 2017-11-25
 */
@SuppressWarnings("WeakerAccess")
public class QueryBuilder<T> implements AutoCloseable {

    final private Logger logger = Logger.getLogger(this.getClass().getName());

    private PersistenceInstance persistenceInstance;

    final private EntityManager entityManager;
    final private CriteriaBuilder criteriaBuilder;
    final private CriteriaQuery<T> query;
    final private Root<T> root;

    private List<Predicate> predicates = new ArrayList<>();
    private List<QueryCriteria> criteriaArrayList = new ArrayList<>();

    private TypedQuery<T> typedQuery; // defer to getter internally as this value may be null.

    /**
     * <p>Concrete enums to offer for sorting.</p>
     */
    public enum Sort {
        ASC, DESC
    }

    /**
     * <p>Provides a {@link QueryBuilder} without the need of creating or having
     * a {@link PersistenceInstance}. When used in a try... block the underlying
     * connection is closed when the resource is out of scope.</p>
     *
     * @param clazz the type of results to be expected when {@link #get()} is called
     */
    public QueryBuilder(final Class<T> clazz) {
        this.persistenceInstance = new PersistenceInstance();

        this.entityManager = persistenceInstance.getEntityManager();
        this.criteriaBuilder = entityManager.getCriteriaBuilder();
        this.query = criteriaBuilder.createQuery(clazz);
        this.root = query.from(clazz);
    }

    /**
     * <p>Provides a {@link QueryBuilder} without the need of creating or having
     * a {@link PersistenceInstance}. When used in a try... block the underlying
     * connection is closed when the resource is out of scope.</p>
     *
     * @param clazz             the type of results to be expected when {@link #get()} is called
     * @param entityFactoryName if there are many managed {@link EntityManager}
     *                          the overload allows for specific manager to be
     *                          used
     */
    public QueryBuilder(final Class<T> clazz, final String entityFactoryName) {
        this.persistenceInstance = new PersistenceInstance(entityFactoryName);

        this.entityManager = persistenceInstance.getEntityManager();
        this.criteriaBuilder = entityManager.getCriteriaBuilder();
        this.query = criteriaBuilder.createQuery(clazz);
        this.root = query.from(clazz);
    }

    /**
     * <p>Starting point for building a query with a defined {@link PersistenceInstance}.</p>
     *
     * @param clazz               the type of results to be expected when {@link #get()} is called
     * @param persistenceInstance the {@link PersistenceInstance} containing a {@link EntityManager}
     */
    public QueryBuilder(final Class<T> clazz, final PersistenceInstance persistenceInstance) {
        this.entityManager = persistenceInstance.getEntityManager();
        this.criteriaBuilder = entityManager.getCriteriaBuilder();
        this.query = criteriaBuilder.createQuery(clazz);
        this.root = query.from(clazz);
    }

    /**
     * <p>Starting point for building a query when a {@link EntityManager} is
     * provided.</p>
     *
     * @param clazz         the type of results to be expected when {@link #get()} is called
     * @param entityManager a managed {@link EntityManager}
     */
    public QueryBuilder(final Class<T> clazz, final EntityManager entityManager) {
        this.entityManager = entityManager;
        this.criteriaBuilder = entityManager.getCriteriaBuilder();
        this.query = criteriaBuilder.createQuery(clazz);
        this.root = query.from(clazz);
    }

    /**
     * <p>Enables performing a left join from the root {@link T}.</p>
     *
     * @param key   the attribute to be joined on
     * @param value the alias that will represent the attribute
     * @return this {@link QueryBuilder}
     */
    public QueryBuilder<T> createAlias(String key, String value) {
        root.join(key, JoinType.LEFT).alias(value);
        return this;
    }

    /**
     * <p>Creates an equality {@link Predicate} that is added to the {@link CriteriaBuilder} list of
     * {@link Predicate} that will be used in the final {@link TypedQuery}.</p>
     *
     * @param property the java object property name to be queried on
     * @param value    the value that the java object is expected to equal
     * @return this {@link QueryBuilder}
     */
    public QueryBuilder<T> eq(final String property, final Object value) {
        logger.finest(() -> String.format("Creating a new eq predicate. key: %s value: %s",
            property, value));
        ParameterExpression expression = criteriaBuilder.parameter(value.getClass());

        QueryCriteria queryCriteria = new QueryCriteria<>(property, value);
        queryCriteria.setExpression(expression);

        criteriaArrayList.add(queryCriteria);

        predicates.add(criteriaBuilder.equal(root.get(property), expression));
        return this;
    }

    /**
     * <p>Expresses a not equals expression.</p>
     *
     * @param property the class property to be inspected
     * @param value    the value that the property should not be equal to
     * @return this {@link QueryBuilder}
     */
    public QueryBuilder<T> notEq(final String property, final Object value) {
        logger.finest(() -> String.format("Creating a notEq predicate. key: %s value: %s",
            property, value));

        ParameterExpression expression = criteriaBuilder.parameter(value.getClass());

        QueryCriteria criteria = new QueryCriteria<>(property, value);
        criteria.setExpression(expression);

        criteriaArrayList.add(criteria);

        predicates.add(criteriaBuilder.notEqual(root.get(property), expression));
        return this;
    }

    /**
     * <p>Expresses a greater than expression. Note that it really only makes
     * sense to provide a {@link Number} to this method; an object has been used
     * instead to allow the generic of {@link QueryCriteria#value}.</p>
     *
     * @param property the class property to be inspected
     * @param value    the value that the property should be greater than
     * @return this {@link QueryBuilder}
     */
    public QueryBuilder<T> gt(final String property, final Object value) {
        logger.finest(() -> String.format("Creating a notEq predicate. key: %s value: %s", property, value));

        ParameterExpression expression = criteriaBuilder.parameter(value.getClass());

        QueryCriteria criteria = new QueryCriteria<>(property, value);
        criteria.setExpression(expression);

        criteriaArrayList.add(criteria);

        predicates.add(criteriaBuilder.gt(root.get(property), expression));
        return this;
    }

    /**
     * <p>Expresses a less than expression. Note that it really only makes
     * sense to provide a {@link Number} to this method; an object has been used
     * instead to allow the generic of {@link QueryCriteria#value}.</p>
     *
     * @param property the class property to be inspected
     * @param value    the value that the property should be less than
     * @return this {@link QueryBuilder}
     */
    public QueryBuilder<T> lt(final String property, final Object value) {
        logger.finest(() -> String.format("Creating a notEq predicate. key: %s value: %s", property, value));

        ParameterExpression expression = criteriaBuilder.parameter(value.getClass());

        QueryCriteria criteria = new QueryCriteria<>(property, value);
        criteria.setExpression(expression);

        criteriaArrayList.add(criteria);

        predicates.add(criteriaBuilder.lt(root.get(property), expression));
        return this;
    }

    /**
     * <p>Creates a like {@link Predicate} that is added to the {@link CriteriaBuilder} list of
     * {@link Predicate} that will be used in the final {@link TypedQuery}.</p>
     *
     * @param property the java object property name to be queried on
     * @param pattern  the pattern that the {@link String} is supposed to support
     * @return this {@link QueryBuilder}
     */
    public QueryBuilder<T> like(final String property, final CharSequence pattern) {
        logger.finest(() -> String.format("Creating new like predicate. key: %s pattern: %s",
            property, pattern));
        ParameterExpression<String> expression = criteriaBuilder.parameter(String.class);

        QueryCriteria queryCriteria = new QueryCriteria<>(property, pattern);
        queryCriteria.setExpression(expression);

        criteriaArrayList.add(queryCriteria);

        predicates.add(criteriaBuilder.like(root.get(property), expression));
        return this;
    }

    /**
     * <p>Creates a not like {@link Predicate} that is added to the {@link CriteriaBuilder} list of
     * {@link Predicate} that will be used in the final {@link TypedQuery}.</p>
     *
     * @param property the java object property name to be queried on
     * @param pattern  the pattern that the {@link String} is supposed to support
     * @return this {@link QueryBuilder}
     */
    public QueryBuilder<T> notLike(final String property, final CharSequence pattern) {
        logger.finest(() -> String.format("Creating new not like predicate. key: %s pattern: %s",
            property, pattern));
        ParameterExpression<String> expression = criteriaBuilder.parameter(String.class);

        QueryCriteria queryCriteria = new QueryCriteria<>(property, pattern);
        queryCriteria.setExpression(expression);

        criteriaArrayList.add(queryCriteria);

        predicates.add(criteriaBuilder.notLike(root.get(property), expression));
        return this;
    }

    /**
     * <p>Creates a or {@link Predicate} that is added to the {@link CriteriaBuilder} list of
     * {@link Predicate} that will be used in the final {@link TypedQuery}</p>
     *
     * @param queryCriteria an array of {@link QueryCriteria}
     * @return this {@link QueryBuilder}
     */
    public QueryBuilder<T> or(final QueryCriteria... queryCriteria) {
        logger.finest(() -> "Creating new or predicate.");
        List<Predicate> orPredicates = new ArrayList<>();

        for (QueryCriteria criteria : queryCriteria) {
            logger.finest(() -> String.format("Adding eq predicate to or conditional. key: %s value: %s",
                criteria.getProperty(), criteria.getValue()));
            ParameterExpression expression =
                criteriaBuilder.parameter(criteria.getValue().getClass());

            criteria.setExpression(expression);

            criteriaArrayList.add(criteria);

            switch (criteria.getComparator()) {
                case EQUALS:
                    orPredicates.add(criteriaBuilder.equal(root.get(criteria.getProperty()), expression));
                    break;
                case LIKE:
                    orPredicates.add(criteriaBuilder.like(root.get(criteria.getProperty()), expression));
                    break;
                case IS_NULL:
                    orPredicates.add(criteriaBuilder.isNull(root.get(criteria.getProperty())));
                    break;
                case IS_NOT_NULL:
                    orPredicates.add(criteriaBuilder.isNotNull(root.get(criteria.getProperty())));
                    break;
                case NOT_EQUALS:
                    orPredicates.add(criteriaBuilder.notEqual(root.get(criteria.getProperty()), criteria.getValue()));
                    break;
                case GREATER_THAN:
                    orPredicates.add(criteriaBuilder.gt(root.get(criteria.getProperty()), (Number) criteria.getValue()));
                    break;
                default:
                    orPredicates.add(criteriaBuilder.equal(root.get(criteria.getProperty()), expression));
                    break;
            }
        }

        predicates.add(criteriaBuilder.or(orPredicates.toArray(new Predicate[orPredicates.size()])));
        return this;
    }

    /**
     * <p>Delegates to {@link #or(QueryCriteria[])} permitting clients to use either a {@link List}
     * or a array of {@link QueryCriteria} items.</p>
     *
     * @param queryCriteria a {@link List} of {@link QueryCriteria}
     * @return this {@link QueryBuilder}
     */
    public QueryBuilder<T> or(final List<QueryCriteria> queryCriteria) {
        logger.finest(() -> "Defaulting to array type or builder.");
        return or(queryCriteria.toArray(new QueryCriteria[queryCriteria.size()]));
    }

    /**
     * <p>Provides sorting support asking the ORM to ask the database to sort
     * the data before returning it to the caller.</p>
     *
     * @param column the column to be sorted
     * @param order  the order to be used
     * @return
     */
    public QueryBuilder<T> sortBy(final String column, final String order) {
        switch (order) {
            case "asc":
                query.orderBy(criteriaBuilder.asc(root.get(column)));
                break;
            case "desc":
                query.orderBy(criteriaBuilder.desc(root.get(column)));
                break;
        }

        return this;
    }

    /**
     * <p>Provides sorting support based on enums asking the ORM to ask the
     * database to sort the data before returning it to the caller.</p>
     *
     * @param column the column to be sorted
     * @param order  the order to be used
     * @return this {@link QueryBuilder}
     */
    public QueryBuilder<T> sortBy(final String column, final Sort order) {
        switch (order) {
            case ASC:
                query.orderBy(criteriaBuilder.asc(root.get(column)));
                break;
            case DESC:
                query.orderBy(criteriaBuilder.desc(root.get(column)));
                break;
        }

        return this;
    }

    /**
     * <p>Permits the declaration where a property is null.</p>
     *
     * @param property the property that must be null in the database
     * @return this {@link QueryBuilder}
     */
    public QueryBuilder<T> isNull(final String property) {
        logger.finest(() -> String.format("Adding a conditional isNull for property: %s", property));
        predicates.add(criteriaBuilder.isNull(root.get(property)));
        return this;
    }

    /**
     * <p>Permits the declaration where property is not null.</p>
     *
     * @param property the property that must not be null in the database
     * @return this {@link QueryBuilder}
     */
    public QueryBuilder<T> isNotNull(final String property) {
        logger.finest(() -> String.format("Adding a conditional isNotNull for property: %s", property));
        predicates.add(criteriaBuilder.isNotNull(root.get(property)));
        return this;
    }

    /**
     * <p>Allows for a {@link TypedQuery} to only return the declared number of values. Providing a
     * negative value throws a {@link IllegalArgumentException}.</p>
     *
     * @param limit a non negative value determining the max number of results
     * @return this {@link QueryBuilder} object
     */
    public QueryBuilder<T> limit(int limit) {
        logger.finest(() -> String.format("Max number of results for the typed query is: %s", limit));
        getTypedQuery().setMaxResults(limit);
        return this;
    }

    /**
     * <p>Permits for offsets which permits pagination of a tables results. Note
     * that when using this method {@link #limit(int)} is implicitly called!</p>
     *
     * @param page     the page location that a user is in
     * @param pageSize the number of items that are permitted in a page
     * @return this {@link QueryBuilder}
     */
    public QueryBuilder<T> page(int page, int pageSize) {
        logger.finest(() -> String.format("Declaring a query offset. page: %s pageSize: %s",
            page, pageSize));
        limit(pageSize);
        getTypedQuery().setFirstResult(page * pageSize);
        return this;
    }

    /**
     * <p>The final point to obtaining the {@link List} of items a user is performing a query
     * on.</p>
     *
     * @return the {@link List} of objects of type {@link T}
     */
    public List<T> get() {
        logger.finest(() -> "Defining values for declared predicates.");
        for (QueryCriteria criteria : criteriaArrayList) {
            getTypedQuery().setParameter(criteria.getExpression(), criteria.getValue());
        }

        logger.finest("Running query.");
        return getTypedQuery().getResultList();
    }

    /**
     * <p>The {@link TypedQuery} object that is used for the final query. If the provided API does
     * not a method for your task you can define these needed items on the {@link TypedQuery} object
     * itself prior to {@link #get()}.</p>
     *
     * @return the {@link QueryBuilder} level {@link TypedQuery}
     */
    public TypedQuery<T> getTypedQuery() {
        if (typedQuery == null) {
            logger.finest(() -> "Consuming all predicates and adding them to the where clause.");
            query.where(criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()])));

            typedQuery = entityManager.createQuery(query);
        }

        return typedQuery;
    }

    @Override
    public void close() {
        if (persistenceInstance != null) this.persistenceInstance.close();
    }
}
