package com.projects4fun.server.sql.query;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Predicate;

/**
 * <p>A middle point container for bridging the {@link Predicate} and a
 * {@link TypedQuery}. The class holds the {@link ParameterExpression} that
 * needs to be bound later when running the actual query.</p>
 *
 * @author Juan Garcia
 * @since 2017-11-25
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class QueryCriteria<E> {

    public enum Comparator {
        IS_NOT_NULL, IS_NULL, EQUALS, LIKE, NOT_EQUALS, GREATER_THAN, LESS_THAN
    }

    private String property;
    private E value;
    private Comparator comparator = Comparator.EQUALS;
    private ParameterExpression expression;

    /**
     * <p>The default constructor to be used when using libraries such as jackson
     * for deserializers into a Java object and reduce the need for wrapper
     * objects around this object. The primary target of this class are tools
     * such as Jackson for JSON or JAXB for XML.</p>
     */
    public QueryCriteria() {
    }

    /**
     * <p>A constructor to permit declaration of {@link #property} instances
     * that do not require a expression to be defined when using JPA. When this
     * constructor is used property must not be null. Such items in JPA are
     * isNull and isNotNull.</p>
     *
     * @param property the java object property name to be queried on
     */
    public QueryCriteria(final String property) {
        if (property == null) {
            throw new IllegalArgumentException("property must be defined.");
        }

        this.property = property;
    }

    /**
     * <p>A constructor to permit declaration of {@link #property} and
     * {@link #value} that require an expression to be defined when using JPA.
     * When this constructor is used both property and value must not be null.</p>
     *
     * @param property the java object property name to be queried on
     * @param value    the value that the java object is expected to equal
     */
    public QueryCriteria(final String property, final E value) {
        if (property == null || value == null) {
            throw new IllegalArgumentException("property and value must be defined.");
        }

        this.property = property;
        this.value = value;
    }

    /**
     * <p>A constructor with a complete definition of all options under the
     * {@link QueryCriteria}. The constructor is a quality of life addition to
     * ease one line object creation.</p>
     *
     * @param property   the java object property name to be queried on
     * @param value      the value that the java object is expected to equal
     * @param comparator the comparison to be used to inspect a value
     */
    public QueryCriteria(final String property, final E value, final Comparator comparator) {
        if (property == null || value == null || comparator == null) {
            throw new IllegalArgumentException("property, value, and comparator must be defined.");
        }

        this.property = property;
        this.value = value;
        this.comparator = comparator;
    }

    /**
     * <p>Gets the currently defined property name</p>
     *
     * @return currently defined property name
     */
    public String getProperty() {
        return property;
    }

    /**
     * <p>Defines the class property field name that is going to be queried for</p>
     *
     * @param property property field name
     */
    public void setProperty(String property) {
        this.property = property;
    }

    /**
     * <p>The currently defined value.</p>
     *
     * @return currently defined value
     */
    public E getValue() {
        return value;
    }

    /**
     * <p>The value to be used when it is time to perform a JPA query.</p>
     *
     * @param value a target query value
     */
    public void setValue(E value) {
        this.value = value;
    }

    /**
     * <p>The defined comparator. If not defined sets the comparator to {@link Comparator#EQUALS}
     * and returns that value.</p>
     *
     * @return the currently defined comparator
     */
    public Comparator getComparator() {
        if (comparator == null) {
            comparator = Comparator.EQUALS;
        }

        return comparator;
    }

    /**
     * <p>Sets the comparator to be used. A null comparator is never allowed an attempt to declare
     * such an item gets defined to {@link Comparator#EQUALS}.</p>
     *
     * @param comparator the {@link Comparator} to be used
     */
    public void setComparator(final Comparator comparator) {
        if (comparator == null) {
            return;
        }

        this.comparator = comparator;
    }

    /**
     * <p>Internal expression holder. Intended audience are JPA queries.</p>
     *
     * @return the defined {@link ParameterExpression}
     */
    public ParameterExpression getExpression() {
        return expression;
    }

    /**
     * <p>Internal expression holder. Intended audience are JPA queries.</p>
     *
     * @param expression the expression to be assigned to this criteria item
     */
    public void setExpression(ParameterExpression expression) {
        this.expression = expression;
    }
}
