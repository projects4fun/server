package com.projects4fun.server.sql.jpa;

import javax.persistence.EntityManager;

/**
 * <p>Convenience class that begins, commits, and closes the {@link EntityManager} transaction. The
 * method is meant to be a convenience method that does not require one to litter their transaction
 * methods with transaction preparations.</p>
 *
 * @author Juan Garcia
 * @since 2017-11-27
 */
public class PersistenceTransaction implements AutoCloseable {

    private EntityManager entityManager;

    /**
     * <p>Default constructor that obtains the default {@link EntityManager}.</p>
     */
    public PersistenceTransaction() {
        this.entityManager = PersistenceManager.getEntityManager();
        entityManager.getTransaction().begin();
    }

    /**
     * <p>Constructor that obtains a named {@link EntityManager}.</p>
     *
     * @param entityFactoryName the unique name to identify the {@link EntityManager}
     */
    public PersistenceTransaction(final String entityFactoryName) {
        this.entityManager = PersistenceManager.getEntityManager(entityFactoryName);
        entityManager.getTransaction().begin();
    }

    /**
     * <p>Provides access to the {@link EntityManager} created by this methods constructor.</p>
     *
     * @return the {@link EntityManager} associated to this class
     */
    public EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * <p>Commits the actions and closes this {@link EntityManager}.</p>
     */
    @Override
    public void close() {
        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
