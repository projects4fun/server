package com.projects4fun.server.sql.jdbc;

import com.projects4fun.server.sql.jpa.PersistenceConfiguration;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * <p>Provides a static means of accessing {@link Connection} objects from either the default data
 * source pool or a named data source pool.</p>
 *
 * @author Juan Garcia
 * @since 2017-11-25
 */
public class ConnectionPool {

    final private static Logger logger = Logger.getLogger(ConnectionPool.class.getName());

    private static HashMap<String, HikariDataSource> dataSourceHashMap = new HashMap<>();

    /**
     * <p>Creates a {@link HikariDataSource} with the default internal name. This method should be
     * called in those instances in which only one {@link ConnectionPool} exists.</p>
     *
     * @param url      the jdbc url to access the database.
     * @param username the username to connect to the database
     * @param password the password to access the database
     */
    public static synchronized void createPool(final String url, final String username,
                                               final String password) {
        createPool(url, username, password, PersistenceConfiguration.DEFAULT_CONNECTION_NAME);
    }

    /**
     * <p>Creates a {@link HikariDataSource} where the datasource will be identified by the provided
     * {@link String} named "sourceId".</p>
     *
     * @param url      the jdbc url to access the database.
     * @param username the username to connect to the database
     * @param password the password to access the database
     * @param sourceId the id used to identify the created data source
     */
    public static synchronized void createPool(final String url, final String username,
                                               final String password, final String sourceId) {
        logger.finer(() -> String.format("Creating connection pool for: %s", url));
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl(url);
        hikariConfig.setUsername(username);
        hikariConfig.setPassword(password);

        HikariDataSource dataSource = new HikariDataSource(hikariConfig);

        dataSourceHashMap.put(sourceId, dataSource);
    }

    /**
     * <p>Obtains a {@link Connection} from the {@link #dataSourceHashMap}. Prior to calling this
     * method a pool of connections should have been created.</p>
     *
     * @return an object of {@link Connection}
     * @throws SQLException if we are unable to {@link Connection} object; if the database is down
     */
    public static Connection getConnection() throws SQLException {
        logger.finest(() -> "Delegating to named get connection method from default get connection.");
        return dataSourceHashMap.get(PersistenceConfiguration.DEFAULT_CONNECTION_NAME)
                .getConnection();
    }

    /**
     * <p>Obtains a {@link Connection} from the {@link #dataSourceHashMap} identified by a
     * {@link String}. The called argument should have been provided at a previous point via a
     * createPool method.</p>
     *
     * @param sourceId the data source id that was used to create this connection pool
     * @return an object of {@link Connection}
     * @throws SQLException if we are unable to {@link Connection} object; if the database is down
     */
    public static Connection getConnection(final String sourceId) throws SQLException {
        logger.finest(() -> String.format("Obtaining connection for: %s", sourceId));
        if (sourceId == null) {
            throw new IllegalArgumentException("A null sourceId is not valid.");
        }

        if (dataSourceHashMap.containsKey(sourceId)) {
            return dataSourceHashMap.get(sourceId).getConnection();
        } else {
            throw new IllegalArgumentException("An sourceId with the name " + sourceId
                    + " was not found.");
        }
    }

    /**
     * <p>Exposes the default {@link DataSource} this application is configured to use.</p>
     *
     * @return the default {@link DataSource}
     */
    public static DataSource getDataSource() {
        logger.finest(() -> "Obtaining default DataSource interface.");
        return dataSourceHashMap.get(PersistenceConfiguration.DEFAULT_CONNECTION_NAME);
    }

    /**
     * <p>Exposes the {@link DataSource} that can be used to let third party components access data
     * from our pooled data set.</p>
     *
     * @param sourceId the name of the {@link HikariDataSource} to obtain a {@link DataSource} from
     * @return the behind the scenes {@link DataSource}
     */
    public static DataSource getDataSource(final String sourceId) {
        logger.finest(() -> String.format("Obtaining named DataSource interface with sourceId: %s", sourceId));
        if (sourceId == null) {
            throw new IllegalArgumentException("A null sourceId is not valid.");
        }

        for (Map.Entry<String, HikariDataSource> dataSourceEntry : dataSourceHashMap.entrySet()) {
            logger.finest(() -> String.format("Identified a DataSource entry of: %s", dataSourceEntry.getKey()));
            if (dataSourceEntry.getKey().equals(sourceId)) {
                return dataSourceEntry.getValue();
            }
        }

        throw new IllegalArgumentException("An sourceId with the name " + sourceId
                + " was not found.");
    }

    /**
     * <p>Permits the closing of a {@link HikariDataSource}. This should be called in a environment
     * where multiple database are being connected to.</p>
     *
     * @param sourceId the connection pool that needs to be shutdown.
     */
    public static synchronized void close(final String sourceId) {
        logger.finest(() -> String.format("Closing the ConnectionPool for sourceId: %s", sourceId));
        if (sourceId == null) {
            throw new IllegalArgumentException("A null entity factory name is not valid.");
        }

        logger.finest(() -> String.format("Searching data source map for: %s", sourceId));
        if (dataSourceHashMap.containsKey(sourceId)) {
            dataSourceHashMap.get(sourceId).close();
            dataSourceHashMap.remove(sourceId);
        } else {
            throw new IllegalArgumentException("Provided entity factory was not found.");
        }
    }

    /**
     * <p>Closes all {@link HikariDataSource} instances that have been loaded onto our map.</p>
     */
    public static void close() {
        logger.finest(() -> "Closing all established ConnectionPool instances.");
        for (Map.Entry<String, HikariDataSource> dataSourceEntry : dataSourceHashMap.entrySet()) {
            dataSourceEntry.getValue().close();
        }
    }
}
