package com.projects4fun.server.sql.jpa;

import javax.persistence.EntityManagerFactory;
import java.util.Map;

/**
 * <p>Configures the {@link EntityManagerFactory} in a means that should be
 * reusable across components that need database connectivity.</p>
 *
 * @author Juan Garcia
 * @since 2017-11-25
 */
public interface PersistenceConfiguration {

    String DEFAULT_CONNECTION_NAME = "default";

    /**
     * <p>A name used to identify a {@link EntityManagerFactory} among the pool of
     * {@link EntityManagerFactory}. The {@link PersistenceManager} maintains a
     * {@link Map} of {@link EntityManagerFactory} and provides means of accessing
     * a particular item over named {@link PersistenceInstance} constructor.</p>
     *
     * @return the name to identify a {@link EntityManagerFactory}
     */
    String entityFactoryName();

    /**
     * <p>The name of the persistence unit in persistence.xml at xpath of
     * //persistence-unit/@name used to instruct the application of the data
     * model that it needs to work with.</p>
     *
     * @return the persistence unit name for a {@link EntityManagerFactory}
     */
    String getPersistenceUnitName();

    /**
     * <p>The properties to configure the {@link EntityManagerFactory}.</p>
     *
     * @return properties that will be used to create {@link EntityManagerFactory}
     */
    Map<String, String> getProperties();
}
