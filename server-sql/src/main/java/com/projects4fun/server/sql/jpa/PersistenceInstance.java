package com.projects4fun.server.sql.jpa;

import javax.persistence.EntityManager;

/**
 * <p>A wrapper around {@link EntityManager} providing the {@link AutoCloseable}
 * interface allowing for try with resources.</p>
 *
 * @author Juan Garcia
 * @since 2017-11-25
 */
public class PersistenceInstance implements AutoCloseable {

    private EntityManager entityManager;

    /**
     * <p>Default constructor that obtains the default {@link EntityManager}.</p>
     */
    public PersistenceInstance() {
        this.entityManager = PersistenceManager.getEntityManager();
    }

    /**
     * <p>Constructor that obtains a named {@link EntityManager}.</p>
     *
     * @param entityFactoryName the unique name to identify the {@link EntityManager}
     */
    public PersistenceInstance(final String entityFactoryName) {
        this.entityManager = PersistenceManager.getEntityManager(entityFactoryName);
    }

    /**
     * <p>Provides access to the {@link EntityManager} created by this methods constructor.</p>
     *
     * @return the {@link EntityManager} associated to this class
     */
    public EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    public void close() {
        entityManager.close();
    }
}
