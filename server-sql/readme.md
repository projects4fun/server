#### A SQL artifact

This artifact provides Hibernate and HikariCP as the ORM and connection pool
manager of choice. A JDBC driver is intentionally missing from this package and
should be provided by the software using this dependency.

#### Working with the flyway plugin

The artifact provides flyway as a means of handling database migrations. The 
current recommended means of providing the user, password, and URL is the use of
 a `flyway.properties` or configuring the connectivity in `settings.xml` for maven
a sample `flyway.properties` is present in the parent directory.