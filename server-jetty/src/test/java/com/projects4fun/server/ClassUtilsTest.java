package com.projects4fun.server;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author Juan Garcia
 * @since 2018-04-03
 */
class ClassUtilsTest {

    @Test
    void path_extract_for_class_returns_base_path() {
        String s = ClassUtils.extractDotPath(ClassUtils.class);
        Assertions.assertEquals("com.projects4fun.server", s);
    }

    @Test
    void path_extract_for_null_throws_illegal_argument() {
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                ClassUtils.extractDotPath(null));
    }
}
