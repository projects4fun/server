package com.projects4fun.server.writers.soap;

import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <p>Provides the means to return a {@link SOAPMessage} from servlets set up
 * to respond with "application/soap+xml". This {@link MessageBodyWriter} only
 * supports simple XML.</p>
 *
 * @author Juan Garcia
 * @since 2018-01-07
 */
@Provider
@Produces("application/soap+xml")
public class SOAPMessageBodyWriter implements MessageBodyWriter<SOAPMessage> {

    final private Logger logger = Logger.getLogger(this.getClass().getName());

    /**
     * <p>Validates that the object that is currently at hand can be transformed
     * by the defined writeTo method defined below. This must return true in
     * order for the transformation process to continue.</p>
     *
     * @param type
     * @param genericType
     * @param annotations
     * @param mediaType
     * @return
     */
    @Override
    public boolean isWriteable(Class<?> type, Type genericType,
                               Annotation[] annotations, MediaType mediaType) {
        return SOAPMessage.class.isAssignableFrom(type);
    }

    /**
     * <p>The use of this method is deprecated per the documentation and a
     * implementor should return -1. The size is computed later on the process
     * by jersey in our case as it is the JAX-RS implementation being used.</p>
     *
     * @param soapMessage
     * @param type
     * @param genericType
     * @param annotations
     * @param mediaType
     * @return
     */
    @Override
    public long getSize(SOAPMessage soapMessage, Class<?> type, Type genericType,
                        Annotation[] annotations, MediaType mediaType) {
        return -1;
    }

    /**
     * <p>Provides the object and means to write to the {@link OutputStream}.</p>
     *
     * @param soapMessage
     * @param type
     * @param genericType
     * @param annotations
     * @param mediaType
     * @param httpHeaders
     * @param entityStream
     * @throws IOException
     * @throws WebApplicationException
     */
    @Override
    public void writeTo(SOAPMessage soapMessage, Class<?> type, Type genericType,
                        Annotation[] annotations, MediaType mediaType,
                        MultivaluedMap<String, Object> httpHeaders,
                        OutputStream entityStream)
        throws IOException, WebApplicationException {

        try {
            soapMessage.writeTo(entityStream);
        } catch (SOAPException e) {
            logger.log(Level.SEVERE, "", e);
        }
    }
}
