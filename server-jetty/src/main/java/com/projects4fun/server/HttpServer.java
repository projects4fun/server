package com.projects4fun.server;

import org.eclipse.jetty.http.HttpVersion;
import org.eclipse.jetty.security.AbstractLoginService;
import org.eclipse.jetty.security.ConstraintMapping;
import org.eclipse.jetty.security.ConstraintSecurityHandler;
import org.eclipse.jetty.security.HashLoginService;
import org.eclipse.jetty.security.authentication.BasicAuthenticator;
import org.eclipse.jetty.server.*;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ErrorPageErrorHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.security.Constraint;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.glassfish.jersey.server.ServerProperties;
import org.glassfish.jersey.servlet.ServletContainer;

import javax.servlet.DispatcherType;
import javax.servlet.ServletRequestListener;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <p>Provides a fluent API in regards to starting a new Jetty application server.</p>
 *
 * @author Juan Garcia
 * @since 2017-12-29
 */
@SuppressWarnings("WeakerAccess")
public class HttpServer {

    final private static Map<String, Server> serverMap = new HashMap<>();
    final public static String PLATFORM_SERVER = "PLATFORM_SERVER";

    final private Logger logger = Logger.getLogger(this.getClass().getName());

    private Server server;
    private ServletContextHandler context;

    private HttpServer() {
    }

    /**
     * <p>Creates a new instance of the {@link HttpServer} for configuration.</p>
     *
     * @return this {@link HttpServer}
     */
    public static HttpServer create() {
        HttpServer httpServer = new HttpServer();
        httpServer.server = new Server();
        return httpServer;
    }

    /**
     * <p>The HTTP port that Jetty will be listening on.</p>
     *
     * @param port the HTTP port to bind on
     * @return this {@link HttpServer}
     */
    public HttpServer addHttpListener(final int port) {
        ServerConnector connector = new ServerConnector(server);
        connector.setPort(port);
        server.addConnector(connector);
        return this;
    }

    /**
     * <p>Provides a means to configure a HTTPS listening port. If the provided
     * key pair is a PFX file the context factory will be instructed to use the
     * PKCS12 provider.</p>
     *
     * @param port   the HTTPS port to bind on
     * @param ksFile the java keystore file
     * @param ksPass the password to the java keystore
     * @return this {@link HttpServer}
     */
    public HttpServer addHttpsListener(final int port, final String ksFile, final String ksPass) {
        SslContextFactory factory = new SslContextFactory();
        factory.setKeyStorePath(ksFile);
        factory.setKeyStorePassword(ksPass);

        if (ksFile.toLowerCase().endsWith("pfx")) {
            factory.setKeyStoreType("PKCS12");
        }

        HttpConfiguration httpConfiguration = new HttpConfiguration();
        httpConfiguration.setSecureScheme("https");
        httpConfiguration.setSecurePort(port);

        HttpConfiguration httpsConfiguration = new HttpConfiguration(httpConfiguration);
        httpsConfiguration.addCustomizer(new SecureRequestCustomizer());

        ServerConnector connector = new ServerConnector(server,
            new SslConnectionFactory(factory, HttpVersion.HTTP_1_1.asString()),
            new HttpConnectionFactory(httpsConfiguration));
        connector.setPort(port);
        server.addConnector(connector);
        return this;
    }

    /**
     * <p>Allows for context paths to be defined</p>
     *
     * @param context   the context uri to be exposed
     * @param packageNs the package that should be scanned for sources
     * @return this {@link HttpServer}
     */
    public HttpServer addServlet(final String context, final String packageNs) {
        return addServlet(new ServletConfiguration() {
            @Override
            public String getContextPath() {
                return context;
            }

            @Override
            public String getPackageNs() {
                return packageNs;
            }
        });
    }

    /**
     * <p>Allows for context paths to be defined</p>
     *
     * @param context   the context uri to be exposed
     * @param packageNs the package that should be scanned for sources
     * @param filter    if a filter should be used
     * @return this {@link HttpServer}
     */
    public HttpServer addServlet(final String context, final String packageNs,
                                 final Class filter) {
        return addServlet(new ServletConfiguration() {
            @Override
            public String getContextPath() {
                return context;
            }

            @Override
            public String getPackageNs() {
                return packageNs;
            }

            @Override
            public Class getFilterClass() {
                return filter;
            }
        });
    }

    /**
     * <p>Performs the task of configuring of a {@link ServletHolder} leveraging
     * the implemented interface.</p>
     *
     * @param configuration an implementation of {@link ServletConfiguration} to
     *                      configure the {@link javax.servlet.Servlet}
     * @return this {@link HttpServer}
     */
    public HttpServer addServlet(final ServletConfiguration configuration) {
        logger.finer(() -> String.format("Adding %s to context path with package name of %s.",
            configuration.getContextPath(), configuration.getPackageNs()));

        ServletHolder holder = getContext().addServlet(ServletContainer.class,
            configuration.getContextPath());

        if (configuration.getPackageNs() == null) {
            logger.warning(() -> "Package was not declared. Web services will not work properly!");
        }

        // non recursive provider packages look up method
        holder.setInitParameter(ServerProperties.PROVIDER_PACKAGES,
            configuration.getPackageNs());

        // provider is not necessary. default implementation returns null
        if (configuration.getProviderClass() != null) {
            StringJoiner providerJoiner = new StringJoiner(";");
            for (Class clazz : configuration.getProviderClass()) {
                logger.finer(() -> String.format("Defining provider %s for packageNs %s",
                    clazz.getName(), configuration.getPackageNs()));

                providerJoiner.add(clazz.getName());
            }

            if (!configuration.getProviderClass().isEmpty()) {
                holder.setInitParameter(ServerProperties.PROVIDER_CLASSNAMES,
                    providerJoiner.toString());
            }
        }

        holder.setInitOrder(0);

        // filter is not necessary. default implementation returns null
        if (configuration.getFilterClass() != null) {
            logger.finer(() -> String.format("A filter was declared. Filter class name: %s",
                configuration.getFilterClass().getName()));
            getContext().addFilter(
                configuration.getFilterClass(),
                configuration.getContextPath(),
                EnumSet.of(DispatcherType.INCLUDE, DispatcherType.REQUEST)
            );
        }

        return this;
    }

    /**
     * <p>Allows a path of the URI to be protected with basic authentication.</p>
     *
     * @param site    the name that will be presented to users when attempting login
     * @param config  the path to a file with users in. file format: username: password [,rolename ...]
     * @param uriPath the URI that should be protected
     * @return this {@link HttpServer}
     */
    public HttpServer setBasicAuth(final String site, final String config, final String uriPath) {
        HashLoginService loginService = new HashLoginService(site, config);
        loginService.setHotReload(true);

        setLoginService(loginService, uriPath);

        return this;
    }

    /**
     * <p>Allows a path of the URI to be protected with basic authentication.
     * This method exposes the means to define the {@link AbstractLoginService}
     * to be used in this method allowing for esoteric methods of authentication.</p>
     *
     * @param loginService the implementation to obtain users and roles
     * @param uriPath      the URI that should be protected
     * @return this {@link HttpServer}
     */
    public HttpServer setLoginService(final AbstractLoginService loginService, final String uriPath) {
        // states that we are using HTTP Basic BasicAuthentication
        Constraint constraint = new Constraint(Constraint.__BASIC_AUTH, "user");
        constraint.setAuthenticate(true);

        // the location our constraint is going to apply to
        ConstraintMapping mapping = new ConstraintMapping();
        mapping.setConstraint(constraint);
        mapping.setPathSpec(uriPath);

        ConstraintSecurityHandler securityHandler = new ConstraintSecurityHandler();
        securityHandler.setAuthenticator(new BasicAuthenticator());
        securityHandler.setRealmName("myrealm");
        securityHandler.addConstraintMapping(mapping);
        securityHandler.setLoginService(loginService);

        getContext().setSecurityHandler(securityHandler);
        return this;
    }

    /**
     * <p>Starts the application server in a new thread and returns</p>
     *
     * @return this {@link HttpServer}
     */
    public HttpServer start() {
        return start(PLATFORM_SERVER);
    }

    /**
     * <p>Starts a named jetty application server.</p>
     *
     * @param name the name to be tied to the server
     * @return this {@link HttpServer}
     */
    public HttpServer start(final String name) {
        if (name == null) {
            throw new RuntimeException("Jetty server must be named.");
        }

        serverMap.put(name, server);

        new Thread(() -> {
            try {
                server.start();
                server.join();
            } catch (Exception e) {
                logger.log(Level.SEVERE, "", e);
            }
        }, name).start();
        return this;
    }

    /**
     * <p>Stops a default instance of the jetty application server</p>
     */
    public void stop() {
        stop(PLATFORM_SERVER);
    }


    /**
     * <p>Stops a named instance of the jetty application server</p>
     *
     * @param name name of the server to stop
     */
    public void stop(final String name) {
        try {
            serverMap.get(name).stop();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "", e);
        }
    }

    /**
     * <p>The {@link Server} that this artifact tries to hide.</p>
     *
     * @return the underlying {@link Server}
     */
    public Server getServer() {
        return server;
    }

    /**
     * <p>The {@link ServletContextHandler} that this artifact tries to hide.
     * Note that this method creates a new {@link ServletContextHandler} if one
     * has not been defined. New {@link ServletContextHandler} are not provided
     * out of this API and should a new one be needed {@link #getServer()} could
     * be leveraged to define new ones.</p>
     *
     * @return a {@link ServletContextHandler}
     */
    public ServletContextHandler getContext() {
        if (context == null) {
            context = new ServletContextHandler(ServletContextHandler.SESSIONS);
            context.addServlet(DefaultServlet.class, "/");
            context.setContextPath("/");

            ContextHandlerCollection contexts = new ContextHandlerCollection();
            contexts.setHandlers(new Handler[]{context});
            server.setHandler(contexts);
        }

        return context;
    }

    public HttpServer setServletRequestListener(ServletRequestListener servletRequestListener) {
        getContext().addEventListener(servletRequestListener);
        return this;
    }

    /**
     * <p>Returns the defined context path</p>
     *
     * @return context path
     */
    public String getContextPath() {
        return context.getContextPath();
    }

    /**
     * <p>The root path that will be used for the registered {@link ServletContextHandler}.</p>
     *
     * @param contextPath the name that should be added to the context paths exposed
     * @return this {@link HttpServer}
     */
    public HttpServer setContextPath(final String contextPath) {
        getContext().setContextPath(contextPath);
        return this;
    }

    /**
     * <p>Returns the defined static files path</p>
     *
     * @return static files path
     */
    public String getStaticFiles() {
        return getContext().getResourceBase();
    }

    /**
     * <p>The path where static content can be found.</p>
     *
     * @param path static content path
     * @return this {@link HttpServer}
     */
    public HttpServer setStaticFiles(final String path) {
        getContext().setResourceBase(path);
        return this;
    }

    /**
     * <p>Defines error pages for HTTP response codes</p>
     *
     * @param code the HTTP response code that should be handled
     * @param uri  the URI that should be presented for the error code
     * @return this {@link HttpServer}
     */
    public HttpServer addErrorPage(int code, String uri) {
        if (getContext().getErrorHandler() == null) {
            ErrorPageErrorHandler errorHandler = new ErrorPageErrorHandler();
            getContext().setErrorHandler(errorHandler);
        }

        ((ErrorPageErrorHandler) getContext().getErrorHandler()).addErrorPage(code, uri);

        return this;
    }
}
