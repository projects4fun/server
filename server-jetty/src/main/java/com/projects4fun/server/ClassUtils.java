package com.projects4fun.server;

/**
 * <p>Set of internal utilities to working with instances of classes.</p>
 *
 * @author Juan Garcia
 * @since 2018-04-03
 */
class ClassUtils {

    /**
     * <p>Extracts the base path of where a class can be found. The target usage
     * is for the identification of the controller package for jetty the
     * packageNs argument.</p>
     *
     * @param clazz the class to be queried for
     * @return the package that the class lives in as a {@link String}
     */
    static String extractDotPath(Class clazz) {
        if (clazz == null) {
            throw new IllegalArgumentException("Invalid class type provided.");
        }

        return clazz.getName().substring(0,
            clazz.getName().lastIndexOf("."));
    }
}
