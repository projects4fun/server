package com.projects4fun.server;

import java.util.List;

/**
 * <p>The {@link ServletConfiguration} interface is used to instruct the
 * application container - Jetty which servlets to expose.</p>
 *
 * @author Juan Garcia
 * @since 2017-12-27
 */
public interface ServletConfiguration {

    /**
     * <p>The web context where a service will be exposed at.</p>
     *
     * @return the {@link String} to register at a given context
     */
    String getContextPath();

    /**
     * <p>The package name where the implementing class of this interface can be
     * found.</p>
     *
     * @return the {@link String} of the package name
     */
    String getPackageNs();

    /**
     * <p>If a {@link javax.servlet.Filter} is to be used the artifact should
     * define it and return the class of it.</p>
     *
     * @return a class implementing {@link javax.servlet.Filter}
     */
    default Class getFilterClass() {
        return null;
    }

    /**
     * <p>A custom {@link javax.ws.rs.ext.MessageBodyWriter} can be defined for
     * a packageNs. Return null if the defaults are acceptable.</p>
     *
     * @return a class implementing {@link javax.ws.rs.ext.MessageBodyWriter}
     */
    default List<Class> getProviderClass() {
        return null;
    }
}
