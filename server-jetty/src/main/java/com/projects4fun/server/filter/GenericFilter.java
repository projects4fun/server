package com.projects4fun.server.filter;

import javax.servlet.*;

/**
 * <p>Implementations of {@link Filter} typically only care about the
 * {@link #doFilter(ServletRequest, ServletResponse, FilterChain)} method. This
 * {@link Filter} provides an implementation of the two methods in the interface
 * removing the need to define these in your own filter.</p>
 */
public abstract class GenericFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void destroy() {

    }
}
