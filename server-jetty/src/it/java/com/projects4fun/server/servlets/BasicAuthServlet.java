package com.projects4fun.server.servlets;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

/**
 * @author Juan Garcia
 * @since 2018-04-03
 */
@Path("/basic-auth")
public class BasicAuthServlet {

    @GET
    public Response get() {
        return Response.ok().build();
    }
}
