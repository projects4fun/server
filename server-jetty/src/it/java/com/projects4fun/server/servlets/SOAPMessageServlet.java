package com.projects4fun.server.servlets;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * <p></p>
 *
 * @author Juan Garcia
 * @since 2018-01-07
 */
@Path("/soap")
public class SOAPMessageServlet {

    @POST
    @Path("/xml")
    @Consumes("application/soap+xml")
    @Produces("application/soap+xml")
    public Response read(final String s) {
        try {
            SOAPMessage message = MessageFactory
                .newInstance(SOAPConstants.SOAP_1_2_PROTOCOL)
                .createMessage(null, new ByteArrayInputStream(s.getBytes()));

            return Response.ok().entity(message).build();
        } catch (SOAPException | IOException e) {
            return Response.serverError().build();
        }
    }
}
