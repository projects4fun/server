package com.projects4fun.server;

import com.projects4fun.server.servlets.BasicAuthServlet;
import org.junit.jupiter.api.*;
import org.sparksource.http.client.HttpClient;

import java.util.concurrent.TimeUnit;

/**
 * @author Juan Garcia
 * @since 2018-04-03
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class BasicAuthIt {

    private HttpServer httpServer;

    @BeforeAll
    void setup() {
        try {
            httpServer = HttpServer.create()
                    .addHttpListener(8202)
                    .addServlet("/it/*", ClassUtils.extractDotPath(BasicAuthServlet.class))
                    .setBasicAuth("IT Site", "resources/user.txt", "/it/*")
                    .start();

            Thread.sleep(TimeUnit.SECONDS.toMillis(5));
        } catch (Exception e) {
            Assertions.fail(e);
        }
    }

    @AfterAll
    void cleanup() {
        httpServer.stop();
    }

    @Test
    void basic_authentication_server_does_require_credentials() {
        try (HttpClient client = HttpClient.get("http://localhost:8202/it/basic-auth")) {
            Assertions.assertEquals(401, client.responseCode());
        } catch (Exception e) {
            Assertions.fail(e);
        }
    }
}
