package com.projects4fun.server;

import com.projects4fun.server.writers.soap.SOAPMessageBodyWriter;
import org.junit.jupiter.api.*;
import org.sparksource.http.client.HttpClient;

import java.util.Collections;
import java.util.List;

/**
 * <p></p>
 *
 * @author Juan Garcia
 * @since 2018-01-07
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MessageWriterIT {

    private HttpServer server;

    @BeforeAll
    void setup() {
        server = HttpServer.create()
            .addHttpListener(8101)
            .addServlet(new ServletConfiguration() {
                @Override
                public String getContextPath() {
                    return "/it/*";
                }

                @Override
                public String getPackageNs() {
                    return ClassUtils.extractDotPath(MessageWriterIT.class);
                }

                @Override
                public List<Class> getProviderClass() {
                    return Collections.singletonList(SOAPMessageBodyWriter.class);
                }
            }).start();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            Assertions.fail(e);
        }
    }

    @Test
    @DisplayName("application/soap+xml HTTP request does not return HTTP 500")
    void soap_xml_http_request_does_not_return_http_500() {
        try (HttpClient httpClient = HttpClient.post("http://localhost:8101/it/soap/xml")) {
            httpClient.setRequestHeader("Content-Type", "application/soap+xml")
                .send("<s:Envelope xmlns:s=\"http://www.w3.org/2003/05/soap-envelope\"><s:Header/><s:Body/></s:Envelope>");

            System.out.println(httpClient.body());
            Assertions.assertEquals(200, httpClient.responseCode());
        } catch (Exception e) {
            Assertions.fail(e);
        }
    }

    @AfterAll
    void cleanup() {
        server.stop();
    }
}
