package com.projects4fun.server;

import org.junit.jupiter.api.*;
import org.sparksource.http.client.HttpClient;

/**
 * <p></p>
 *
 * @author Juan Garcia
 * @since 2017-12-29
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class HttpServerIT {

    private HttpServer server;

    @BeforeAll
    void setup() {
        server = HttpServer.create()
            .addHttpListener(8100)
            .start();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            Assertions.fail(e);
        }
    }

    @Test
    @DisplayName("HTTP Server can be started")
    void http_server_can_be_started() {
        try (HttpClient httpRequest = HttpClient.get("http://localhost:8100")) {
            Assertions.assertEquals(404, httpRequest.responseCode());
        } catch (Exception e) {
            Assertions.fail(e);
        }
    }

    @AfterAll
    void cleanup() {
        server.stop();
    }
}
