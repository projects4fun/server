#### A Jetty HTTP Server wrapper

This provides common components that would be used in a Java application server 
as a single dependency. In a nutshell it provides Jetty server core and the 
servlet; jersey for the JAX-RS implementation, and Jackson for working with 
JSON.

##### Programmatic configuration

The configuration can be started by creating an instance of `HttpServer` a basic
HttpServer could for example be started in the following fashion

    HttpServer.create()
        .addHttpListener(8080)
        .addServlet("/api", "com.api.namespace")
        .start();