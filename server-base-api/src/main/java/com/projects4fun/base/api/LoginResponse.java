package com.projects4fun.base.api;

/**
 * <p>In our endpoints the login attempt has always been an outlier from how
 * other API endpoints respond. A common them however has been the sessionId and
 * account that is ultimately shared.</p>
 *
 * @author Juan Garcia
 * @since 2019-02-09
 */
public class LoginResponse<T> {

    private String sessionId;
    private T account;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public T getAccount() {
        return account;
    }

    public void setAccount(T account) {
        this.account = account;
    }
}
