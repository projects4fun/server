package com.projects4fun.base.api;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>Permits a backend to respond with data and optional errors that may have
 * been encountered.</p>
 *
 * @author Juan Garcia
 * @since 2019-01-19
 */
public class Content<T> {

    private boolean ok = true;
    private T data;
    private List<ErrorDetail> errors;

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public List<ErrorDetail> getErrors() {
        return errors;
    }

    public void setError(ErrorDetail error) {
        if (errors == null) {
            errors = new ArrayList<>();
        }

        this.errors.add(error);
        this.ok = false;
    }

    public void setError(Exception e) {
        if (errors == null) {
            errors = new ArrayList<>();
        }

        this.errors.add(new ErrorDetail(e));
        this.ok = false;
    }
}
