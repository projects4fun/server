package com.projects4fun.base.api;

import java.util.UUID;

/**
 * <p>Provides a container that may be used to present to a consumer a issue
 * that may have occurred with the request. This class always populates the
 * {@link #id} with a {@link UUID} if this id is presented in the front end it
 * may be useful to log / store the {@link #toString()} of this method.</p>
 *
 * @author Juan Garcia
 * @since 2019-01-19
 */
public class ErrorDetail {

    private String message;
    private String code;
    private String id;

    public ErrorDetail() {
    }

    /**
     * <p>A convenience constructor that provides a message as an argument.</p>
     *
     * @param message the message to be housed in this object
     */
    public ErrorDetail(String message) {
        this.message = message;
    }

    /**
     * <p>A convenience constructor that uses the message of a {@link Exception}
     * to populate the message of this object.</p>
     *
     * @param e the {@link Exception} that was thrown / caught
     */
    public ErrorDetail(Exception e) {
        this.message = e.getMessage();
    }

    // =======================
    // =======================

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getId() {
        if (id == null) {
            id = UUID.randomUUID().toString();
        }

        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "ErrorDetail{" +
            "message='" + message + '\'' +
            ", code='" + code + '\'' +
            ", id='" + id + '\'' +
            '}';
    }
}
