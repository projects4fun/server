package com.projects4fun.server.pool;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.Closeable;
import java.io.PrintStream;
import java.time.Duration;

/**
 * <p></p>
 *
 * @author Juan Garcia
 * @since 2020-01-04
 */
public class CloseableResourcePoolTest {

    @Test
    void resource_can_be_obtained_from_resource_pool() {
        CloseablePool<DemoResource> pool = buildResourcePool();
        pool.addResource(new DemoResource());

        DemoResource resource = pool.getResource();
        Assertions.assertNotNull(resource);
    }

    @Test
    void multiple_resources_can_be_obtained_from_resource_pool() {
        CloseablePool<DemoResource> pool = buildResourcePool();
        pool.addResource(new DemoResource());
        pool.addResource(new DemoResource());

        Assertions.assertTimeoutPreemptively(Duration.ofSeconds(1), pool::getResource);
        Assertions.assertTimeoutPreemptively(Duration.ofSeconds(1), pool::getResource);
    }

    @Test
    void adding_a_resource_after_pool_is_closed_throws_exception() {
        try {
            CloseablePool<DemoResource> pool = buildResourcePool();
            pool.close();

            Assertions.assertThrows(PoolClosedException.class,
                () -> pool.addResource(new DemoResource()));
        } catch (Exception e) {
            Assertions.fail(e);
        }
    }

    @Test
    void over_allocated_resource_does_block() {
        CloseablePool<DemoResource> pool = buildResourcePool();
        pool.addResource(new DemoResource());

        // simulate some task using the resource
        DemoResource resource = Assertions
            .assertTimeoutPreemptively(Duration.ofSeconds(1), pool::getResource);
        new Thread(() -> {
            try {
                Thread.sleep(Duration.ofSeconds(5).toMillis());
                pool.release(resource);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();

        Assertions.assertTimeoutPreemptively(Duration.ofSeconds(10), pool::getResource);
    }

    @Test
    void released_resource_can_be_used_again() {
        CloseablePool<DemoResource> pool = buildResourcePool();
        pool.addResource(new DemoResource());

        DemoResource resource = pool.getResource();
        pool.release(resource);

        Assertions.assertTimeoutPreemptively(Duration.ofSeconds(1), pool::getResource);
    }

    private CloseablePool<DemoResource> buildResourcePool() {
        return new CloseableResourcePool<>();
    }
}

class DemoResource implements Closeable {
    @Override
    public void close() {
        PrintStream stream = new PrintStream(System.out);
        stream.println("Closing down resource.");
    }
}
