package com.projects4fun.server.security.twofactor;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.concurrent.TimeUnit;

/**
 * <p></p>
 *
 * @author Juan Garcia
 * @since 2018-09-28
 */
class TotpTest {

    @Test
    void generated_two_factor_token_can_be_validated() {
        String secret = Totp.generateSecret();

        long token = Totp.generateToken(secret);
        Assertions.assertTrue(Totp.isValidToken(secret, token, (int)
            TimeUnit.SECONDS.toMillis(15)));
    }
}
