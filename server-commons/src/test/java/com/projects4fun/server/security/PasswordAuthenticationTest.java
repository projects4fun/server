package com.projects4fun.server.security;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author Juan Garcia
 * @since 2019-03-11
 */
class PasswordAuthenticationTest {

    @Test
    void get_instances_provides_pbdkf2_for_hash() {
        final String hash = PBKDF2PasswordAuthentication.TOKEN + "$data";

        PasswordAuthentication pwAuth = PasswordAuthentication.getInstance(hash);

        Assertions.assertSame(pwAuth.getClass(),
            PBKDF2PasswordAuthentication.class);
    }
}
