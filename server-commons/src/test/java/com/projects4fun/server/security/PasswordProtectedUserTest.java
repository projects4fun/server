package com.projects4fun.server.security;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

/**
 * @author Juan Garcia
 * @since 2018-04-06
 */
class PasswordProtectedUserTest {

    private static PBKDF2PasswordAuthentication PBKDF2PasswordAuthentication;

    @BeforeAll
    static void init() {
        PBKDF2PasswordAuthentication = new PBKDF2PasswordAuthentication();
    }

    @Test
    void portal_user_can_be_assigned_a_password() {
        PortalSubject user = new PortalSubject();
        user.setPassword("password");

        PBKDF2PasswordAuthentication.hashPw(user);

        Assertions.assertNotEquals("password", user.getPassword());
    }

    @Test
    void portal_user_without_password_throws_exception() {
        Assertions.assertThrows(IllegalArgumentException.class, () ->
            PBKDF2PasswordAuthentication.hashPw(new PortalSubject()));
    }

    class PortalSubject implements PasswordProtectedSubject {

        private String password;

        @Override
        public String getPassword() {
            return password;
        }

        @Override
        public void setPassword(String password) {
            this.password = password;
        }
    }
}
