package com.projects4fun.server.security;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

/**
 * @author Juan Garcia
 * @since 2018-04-06
 */
class PBKDF2PasswordAuthenticationTest {

    private static PBKDF2PasswordAuthentication PBKDF2PasswordAuthentication;

    @BeforeAll
    static void init() {
        PBKDF2PasswordAuthentication = new PBKDF2PasswordAuthentication();
    }

    @Test
    void password_hash_function_creates_string_hash() {
        Assertions.assertNotNull(PBKDF2PasswordAuthentication.hashPw("password"));
    }

    @Test
    void empty_password_throws_illegal_argument_exception() {
        Assertions.assertThrows(IllegalArgumentException.class, () ->
            PBKDF2PasswordAuthentication.hashPw((String) null));
    }

    @Test
    void empty_salt_throws_illegal_argument_exception() {
        Assertions.assertThrows(IllegalArgumentException.class, () ->
            PBKDF2PasswordAuthentication.hashPw("password", null));
    }

    @Test
    void empty_password_and_salt_throws_illegal_argument_exception() {
        Assertions.assertThrows(IllegalArgumentException.class, () ->
            PBKDF2PasswordAuthentication.hashPw(null, null));
    }

    @Test
    void password_hash_function_allows_custom_salts() {
        Assertions.assertNotNull(PBKDF2PasswordAuthentication.hashPw("password", "salt"));
    }

    @Test
    void password_contains_all_sub_components() {
        String[] components = PBKDF2PasswordAuthentication.hashPw("password").split("\\$");
        Assertions.assertEquals(5, components.length);
    }

    @Test
    void password_is_reproducible_with_known_password() {
        String hash = PBKDF2PasswordAuthentication.hashPw("password");
        Assertions.assertTrue(PBKDF2PasswordAuthentication.isValidPw("password", hash));
    }
}
