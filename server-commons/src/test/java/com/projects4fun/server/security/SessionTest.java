package com.projects4fun.server.security;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.OffsetDateTime;
import java.util.Date;

/**
 * @author Juan Garcia
 * @since 2018-04-10
 */
class SessionTest {

    @Test
    void default_is_valid_session_returns_true_for_current_time() {
        Session session = new SessionImpl(OffsetDateTime.now());
        Assertions.assertTrue(session.isValidSession());
    }

    @Test
    void default_is_valid_session_returns_false_for_old_time() {
        Session session = new SessionImpl(OffsetDateTime.now().minusDays(1));
        Assertions.assertFalse(session.isValidSession());
    }

    class SessionImpl implements Session {

        private OffsetDateTime offsetDateTime;

        SessionImpl(OffsetDateTime offsetDateTime) {
            this.offsetDateTime = offsetDateTime;
        }

        @Override
        public Date getTs() {
            return new Date(offsetDateTime.toInstant().toEpochMilli());
        }
    }
}
