package com.projects4fun.server.pool;

/**
 * <p></p>
 *
 * @author Juan Garcia
 * @since 2020-01-04
 */
public class PoolClosedException extends RuntimeException {
    public PoolClosedException(String message) {
        super(message);
    }
}
