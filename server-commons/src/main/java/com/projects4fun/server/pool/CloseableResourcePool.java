package com.projects4fun.server.pool;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

/**
 * <p></p>
 *
 * @author Juan Garcia
 * @since 2020-01-04
 */
public class CloseableResourcePool<T extends Closeable> implements CloseablePool<T> {

    private List<T> openResource = new ArrayList<>();
    private List<T> lockedResource = new ArrayList<>();
    private boolean isClosed = false;

    final private Object acquireResourceLock = new Object();
    final private Object releaseResourceLock = new Object();

    @Override
    public void addResource(T resource) {
        if (isClosed) {
            throw new PoolClosedException("Pool has been closed.");
        }

        openResource.add(resource);
    }

    @Override
    public void addResource(Supplier<T> tSupplier, int quantity) {
        // a negative or 0 quantity pool makes no sense. exception this case.
        if (quantity < 1) {
            throw new IllegalArgumentException("Quantity must be at least 1.");
        }

        for (int i = 0; i < quantity; i++) {
            addResource(tSupplier.get());
        }
    }

    @Override
    public T getResource() {
        if (isClosed) {
            throw new PoolClosedException("Pool has been closed.");
        }

        synchronized (acquireResourceLock) {
            while (openResource.size() == 0) {
                try {
                    Thread.sleep(25);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }

            // remove from the end to avoid array left shift operation
            T resource = openResource.remove(openResource.size() - 1);
            lockedResource.add(resource);

            return resource;
        }
    }

    @Override
    public void release(T resource) {
        if (isClosed) return;

        synchronized (releaseResourceLock) {
            lockedResource.remove(resource);
            openResource.add(resource);
        }
    }

    @Override
    public void close() throws IOException {
        if (isClosed) return;
        isClosed = true;

        for (T resource : openResource) {
            resource.close();
        }

        openResource = null;

        for (T resource : lockedResource) {
            resource.close();
        }

        lockedResource = null;
    }
}
