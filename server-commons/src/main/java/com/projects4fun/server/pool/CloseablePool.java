package com.projects4fun.server.pool;

import java.io.Closeable;
import java.util.function.Supplier;

/**
 * <p>Provides a interface for managing a pool of resources. The interface
 * provides support for adding, obtaining, releasing, and closing all resources.
 * It is meant to be used for those for those objects that are expensive to
 * create / not thread safe.</p>
 *
 * <p>A client must take care to release a resource after it is done using it.
 * Failure to do so will lock an application until a resource is available.</p>
 *
 * @author Juan Garcia
 * @since 2020-01-04
 */
public interface CloseablePool<T extends Closeable> extends Closeable {

    /**
     * <p>Adds a resource to the available pool.</p>
     *
     * <p>Attempting to add a resource to a pool after it has been closed is a
     * runtime exception.</p>
     *
     * @param resource the resource to be added to the available pool
     * @throws PoolClosedException if this pool has been closed
     */
    void addResource(T resource);

    /**
     * <p>Allows a client to state how a resource will be created. The resource
     * provided by the supplier is added to the available pool.</p>
     *
     * <p>Attempting to add a resource to a pool after it has been closed is a
     * runtime exception.</p>
     *
     * @param tSupplier an implementation with a means to obtain resources
     * @param quantity  the number of
     * @throws PoolClosedException if this pool has been closed
     */
    void addResource(Supplier<T> tSupplier, int quantity);

    /**
     * <p>Obtains a resource from the available pool.</p>
     *
     * <p>Attempting to obtain a resource from a closed pool is a runtime
     * exception.</p>
     *
     * @return a ready to use resource
     * @throws PoolClosedException if this pool has been closed
     */
    T getResource();

    /**
     * <p>Releases a resource back into the available pool.</p>
     *
     * <p>If a pool has been shutdown no operation will be performed. The
     * implementation close takes care of shutting down all resources.</p>
     *
     * @param resource the object being made available again
     */
    void release(T resource);
}
