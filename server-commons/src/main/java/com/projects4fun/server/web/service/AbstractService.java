package com.projects4fun.server.web.service;

import com.projects4fun.server.sql.query.QueryBuilder;
import com.projects4fun.server.sql.query.QueryCriteria;
import com.projects4fun.server.web.query.Parameters;
import com.projects4fun.server.web.query.Query;
import com.projects4fun.server.web.query.Sorting;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>Services do not concern themselves with whether a user contains access to
 * a set of data. Once a request makes it to a service the only concern in place
 * is the type of data that should be presented to a user.</p>
 *
 * @author Juan Garcia
 * @since 2018-12-07
 */
public abstract class AbstractService<E> {

    /**
     * <p>This method prepares dynamic front end queries using {@link Parameters}
     * as the foundation. This method should only be used from the /query
     * endpoints that may require more specialized content compared to the generic
     * CRUD endpoints.</p>
     *
     * @param parameters   front end parameters to build the query
     * @param queryBuilder on instance of query builder ready to run a query
     */
    protected void runQuery(Parameters parameters, QueryBuilder<E> queryBuilder) {
        // support joins
        for (Map.Entry<String, String> entry : parameters.getAliases().entrySet()) {
            if (StringUtils.isNotBlank(entry.getKey()) && StringUtils.isNotBlank(entry.getValue())) {
                queryBuilder.createAlias(entry.getKey(), entry.getValue());
            }
        }

        // standard and clause
        if (!parameters.getColumnQuery().isEmpty()) {
            for (Query query : parameters.getColumnQuery()) {
                if (StringUtils.isNotBlank(query.getField()) && query.getValue() != null && StringUtils.isNotBlank(query.getComparator())) {
                    switch (query.getComparator()) {
                        case "eq":
                            queryBuilder.eq(query.getField(), query.getValue());
                            break;
                        case "like":
                            queryBuilder.like(query.getField(), (String) query.getValue());
                            break;
                        case "ne":
                            queryBuilder.notEq(query.getField(), query.getValue());
                            break;
                        case "gt":
                            queryBuilder.gt(query.getField(), query.getValue());
                            break;
                        case "lt":
                            queryBuilder.lt(query.getField(), query.getValue());
                            break;

                    }
                } else if (StringUtils.isNotBlank(query.getField()) && StringUtils.isNotBlank(query.getComparator()) && StringUtils.equals("isNull", query.getComparator())) {
                    queryBuilder.isNull(query.getField());
                } else if (StringUtils.isNotBlank(query.getField()) && StringUtils.isNotBlank(query.getComparator()) && StringUtils.equals("isNotNull", query.getComparator())) {
                    queryBuilder.isNotNull(query.getField());
                }
            }
        }

        // standard or clause
        if (!parameters.getGlobalQuery().isEmpty()) {
            List<QueryCriteria> criteria = new ArrayList<>();

            for (Query query : parameters.getColumnQuery()) {
                if (StringUtils.isNotBlank(query.getField()) && query.getValue() != null && StringUtils.isNotBlank(query.getComparator())) {
                    switch (query.getComparator()) {
                        case "eq":
                            criteria.add(new QueryCriteria<>(query.getField(), query.getValue(), QueryCriteria.Comparator.EQUALS));
                            break;
                        case "like":
                            criteria.add(new QueryCriteria<>(query.getField(), query.getValue(), QueryCriteria.Comparator.LIKE));
                            break;
                        case "ne":
                            criteria.add(new QueryCriteria<>(query.getField(), query.getValue(), QueryCriteria.Comparator.NOT_EQUALS));
                            break;
                        case "gt":
                            criteria.add(new QueryCriteria<>(query.getField(), query.getValue(), QueryCriteria.Comparator.GREATER_THAN));
                            break;
                        case "lt":
                            criteria.add(new QueryCriteria<>(query.getField(), query.getValue(), QueryCriteria.Comparator.LESS_THAN));
                    }
                } else if (StringUtils.isNotBlank(query.getField()) && StringUtils.isNotBlank(query.getComparator()) && StringUtils.equals("isNull", query.getComparator())) {
                    queryBuilder.isNull(query.getField());
                } else if (StringUtils.isNotBlank(query.getField()) && StringUtils.isNotBlank(query.getComparator()) && StringUtils.equals("isNotNull", query.getComparator())) {
                    queryBuilder.isNotNull(query.getField());
                }
            }

            if (!criteria.isEmpty()) {
                queryBuilder.or(criteria);
            }
        }

        // sorting options
        if (!parameters.getSorting().isEmpty()) {
            for (Sorting sorting : parameters.getSorting()) {
                if (StringUtils.isNotBlank(sorting.getField()) && StringUtils.isNotBlank(sorting.getOrder())) {
                    switch (sorting.getOrder()) {
                        case "asc":
                            queryBuilder.sortBy(sorting.getField(), QueryBuilder.Sort.ASC);
                            break;
                        case "desc":
                            queryBuilder.sortBy(sorting.getField(), QueryBuilder.Sort.DESC);
                            break;
                    }
                }
            }
        }

        // pagination support
        if (parameters.getMax() > 0) {
            queryBuilder.page(parameters.getOffset(), parameters.getMax());
        }
    }
}
