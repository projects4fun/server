package com.projects4fun.server.web.query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>Exposes a parameters object that front ends can use to perform selective
 * queries. Internally the {@link Parameters} object is built using JPA and
 * provide front ends a means to not require hardcoded JPA queries to be defined
 * in the backend.</p>
 *
 * @author Juan Garcia
 * @since 2018-05-02
 */
public class Parameters {

    private List<Sorting> sorting;
    private Map<String, String> aliases;
    private List<Query> columnQuery;
    private List<Query> globalQuery;
    private int max;
    private int offset;

    public List<Sorting> getSorting() {
        if (sorting == null) {
            sorting = new ArrayList<>();
        }

        return sorting;
    }

    public Map<String, String> getAliases() {
        if (aliases == null) {
            aliases = new HashMap<>();
        }

        return aliases;
    }

    public List<Query> getColumnQuery() {
        if (columnQuery == null) {
            columnQuery = new ArrayList<>();
        }

        return columnQuery;
    }

    public List<Query> getGlobalQuery() {
        if (globalQuery == null) {
            globalQuery = new ArrayList<>();
        }

        return globalQuery;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }
}
