package com.projects4fun.server.web.query;

/**
 * <p></p>
 *
 * @author Juan Garcia
 * @since 2018-05-02
 */
public class Sorting {

    private String field;
    private String order;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }
}
