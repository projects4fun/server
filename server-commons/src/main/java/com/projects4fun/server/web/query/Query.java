package com.projects4fun.server.web.query;

/**
 * <p>This object is used to provide the column name, value, and comparator that
 * should be applied to a dynamic query.</p>
 *
 * @author Juan Garcia
 * @since 2018-05-02
 */
public class Query {

    private String field;
    private Object value;
    private String comparator;
    private boolean isDate;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String getComparator() {
        return comparator;
    }

    public void setComparator(String comparator) {
        this.comparator = comparator;
    }

    public boolean isDate() {
        return isDate;
    }

    public void setDate(boolean date) {
        isDate = date;
    }
}
