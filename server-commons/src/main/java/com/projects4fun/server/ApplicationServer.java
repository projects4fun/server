package com.projects4fun.server;

import com.projects4fun.server.configuration.Configuration;
import com.projects4fun.server.configuration.DatabaseAccess;
import com.projects4fun.server.configuration.StaticContent;
import com.projects4fun.server.sql.jpa.PersistenceConfiguration;
import com.projects4fun.server.sql.jpa.PersistenceManager;

import javax.xml.bind.JAXB;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.ServiceLoader;
import java.util.logging.Logger;

/**
 * <p>{@link ApplicationServer} is meant to be extended. An application that wants
 * a default configuration from a "configuration.xml" file in their application
 * home directory performs a single call {@link #loadFromXml()} and will have
 * their application server started.</p>
 *
 * <p>Further configured occurs by overriding {@link #preConfigure(Configuration)}
 * and /or {@link #postConfigure(Configuration)} and using the exposed API or
 * the {@link org.eclipse.jetty.server.Server} could be accessed to configure
 * that which does not have an API.</p>
 */
public abstract class ApplicationServer {

    private Logger logger = Logger.getLogger(this.getClass().getName());

    protected HttpServer server;

    /**
     * <p>Performs the default application server configuration. This method
     * expects for there to be a configuration.xml file in the application home
     * directory.</p>
     */
    public void loadFromXml() {
        loadFromXml(new File("configuration.xml"));
    }

    /**
     * <p>Performs the configuration with a defined path.</p>
     *
     * @param path the location the configuration file can be found at
     */
    public void loadFromXml(final String path) {
        if (path == null) {
            throw new IllegalArgumentException("Path must be provided.");
        }

        loadFromXml(new File(path));
    }

    /**
     * <p>Performs the configuration with a file object. This method as of
     * today performs the entire configuration of an application server from a
     * configuration file.</p>
     *
     * @param path the location the configuration file can be found at
     */
    public void loadFromXml(final File path) {
        if (path == null) {
            throw new IllegalArgumentException("Path must be provided.");
        }

        if (!path.exists()) {
            throw new IllegalArgumentException(path.getAbsolutePath() + " not found!");
        }

        Configuration configuration = JAXB.unmarshal(path, Configuration.class);

        preConfigure(configuration);

        configureDataAccess(configuration.getDatabaseAccess());

        server = HttpServer.create();
        if (configuration.getHttpPort() != null) {
            server.addHttpListener(configuration.getHttpPort());
        }

        if (configuration.getHttpsPort() != null) {
            server.addHttpsListener(configuration.getHttpsPort(),
                configuration.getKeystoreFile(),
                configuration.getKeystorePassword());
        }

        // configure static content page
        {
            StaticContent staticContent = configuration.getStaticContent();
            if (staticContent != null) {
                server.setStaticFiles(staticContent.getPath());
                if (staticContent.getError404Page() != null) {
                    server.addErrorPage(404, staticContent.getError404Page());
                }
            }
        }

        ServiceLoader<ServletConfiguration> serviceLoader =
            ServiceLoader.load(ServletConfiguration.class);
        for (ServletConfiguration servletConfiguration : serviceLoader) {
            logger.info(String.format("Loading component for packageNs %s",
                servletConfiguration.getPackageNs()));
            server.addServlet(servletConfiguration);
        }

        postConfigure(configuration);

        server.start();
    }

    /**
     * <p>Starts and configures a entity manager factory leaves out the need to
     * interact with the JPA interfaces.</p>
     *
     * @param access
     */
    private void configureDataAccess(DatabaseAccess access) {
        if (access == null) {
            logger.fine(() -> "No database connectivity declared. Skipping.");
            return;
        }

        PersistenceManager.initEntityFactory(new PersistenceConfiguration() {
            @Override
            public String entityFactoryName() {
                return DEFAULT_CONNECTION_NAME;
            }

            @Override
            public String getPersistenceUnitName() {
                return access.getPersistenceUnit();
            }

            @Override
            public Map<String, String> getProperties() {
                Map<String, String> properties = new HashMap<>();

                properties.put("javax.persistence.jdbc.url", access.getUrl());
                properties.put("javax.persistence.jdbc.user", access.getUsername());
                properties.put("javax.persistence.jdbc.password", access.getPassword());

                if (access.getDriver() != null) {
                    properties.put("javax.persistence.jdbc.driver", access.getDriver());
                }

                if (access.getDialect() != null) {
                    properties.put("hibernate.dialect", access.getDialect());
                }

                return properties;
            }
        });
    }

    /**
     * <p>Allows for custom functionality to be defined prior to any default
     * configuration taking place. For example could leverage flyway to perform
     * a database migration prior to the entity factory being initialized.</p>
     *
     * @param configuration the {@link Configuration} from the defined file
     */
    public void preConfigure(Configuration configuration) {

    }

    /**
     * <p>Allows for custom functionality to be defined after all default
     * configuration has taken place. For example one could register additional
     * servlets that may not have a {@link ServletConfiguration} extension.</p>
     *
     * @param configuration the {@link Configuration} from the defined file
     */
    public void postConfigure(Configuration configuration) {

    }
}
