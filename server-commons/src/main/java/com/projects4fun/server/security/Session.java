package com.projects4fun.server.security;

import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.Date;

/**
 * <p>In most applications a session is only valid for a particular amount of
 * time. This interface helps provide control for a object claiming to hold a
 * session id. Such a object should maintain a {@link String} for the session
 * and when the particular session id was created.</p>
 *
 * <p>This interface provides two methods to ensure that a session falls within
 * a time constraint. The default validation asks if a session id is within 8
 * hours of creation. The overloaded validation allows a caller to define the
 * period in which a session can be considered valid.</p>
 *
 * @author Juan Garcia
 * @since 2018-04-10
 */
public interface Session {

    /**
     * <p>The time when a session was created.</p>
     *
     * @return creation time of a session
     */
    Date getTs();

    /**
     * <p>Attempts to check if a session falls within 8 hours.</p>
     *
     * @return true if session was created within 8 hours; false otherwise
     */
    default boolean isValidSession() {
        return isValidSession(8, ChronoUnit.HOURS);
    }

    /**
     * <p>Provides a means for a end user to define the period a session is
     * considered to be valid.</p>
     *
     * @param timeToAdd amount of time a session is considered valid
     * @param unit      the unit type to be added to the original creation time; note
     *                  that the original object is copied instead of modified
     * @return true if session was created within time frame; false otherwise
     */
    default boolean isValidSession(final int timeToAdd, final TemporalUnit unit) {
        if (timeToAdd < 0) {
            throw new IllegalArgumentException("timeToAdd may not be negative.");
        }

        if (unit == null) {
            throw new IllegalArgumentException("TemporalUnit may not be null.");
        }

        OffsetDateTime offsetDateTime = OffsetDateTime.now();

        OffsetDateTime creationTime = OffsetDateTime
            .ofInstant(getTs().toInstant(), ZoneId.systemDefault());

        return creationTime.plus(timeToAdd, unit)
            .isAfter(offsetDateTime);
    }
}
