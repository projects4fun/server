package com.projects4fun.server.security.twofactor;

import org.apache.commons.codec.binary.Base32;
import org.apache.commons.text.RandomStringGenerator;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * <p>An implementation of Time-Based One-Time Password (TOTP) based on RFC 6238
 * and 4226. TOTP builds on top of HMAC-based One-Time Password (HOTP). The main
 * distinction is the change around the counter. In HOTP a shared secret and a
 * counter help derive the token to be used where as in TOTP the counter is
 * replaced with the time since epoch.</p>
 *
 * <p>WARNING. Validation and generation of values are dependant on a server
 * with a correct timestamp value. Synchronization of system time using something
 * such as ntp is recommended.</p>
 */
public class Totp {

    // suggested default from rfc 6238
    private static final int DEFAULT_TIME_STEP_SECONDS = 30;
    // from rfc 4648/3548
    private static final char[] PERMITTED_CHARACTERS =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567".toCharArray();

    /**
     * <p>Generate and return a 64-character secret key in base32 format (A-Z2-7)
     * using {@link SecureRandom}. This value should be stored and shared with
     * the user. Other lengths should use {@link #generateSecret(int)}.</p>
     *
     * @return the secret that should be stored and shared with the user
     */
    public static String generateSecret() {
        return generateSecret(64);
    }

    /**
     * <p>Similar to {@link #generateSecret()} but specifies a character
     * length.</p>
     *
     * @param length the length of the key to created
     * @return the secret that should be stored and shared with the user
     */
    public static String generateSecret(int length) {
        Random random = new SecureRandom();

        RandomStringGenerator stringGenerator = new RandomStringGenerator.Builder()
            .selectFrom(PERMITTED_CHARACTERS)
            .usingRandom(random::nextInt)
            .build();

        return stringGenerator.generate(length);
    }

    /**
     * <p>Validate a provided token using the secret base-32 string. This allows
     * you to set a window in milliseconds to account for people being close to
     * the end of the time-step. For example, if windowMillis is 10000 then this
     * method will check the authNumber against the generated number from 10
     * seconds before now through 10 seconds after now.</p>
     *
     * @param secret       secret string encoded using base-32 that was used to
     *                     generate the QR code or shared with the user.
     * @param userToken    time based token provided by the user from their
     *                     authenticator application.
     * @param windowMillis number of milliseconds that are allowed to be off and
     *                     still match. This checks before and after the current
     *                     time to account for clock variance. Set to 0 for no
     *                     window.
     * @return true if the token matched the calculated number within the
     * specified window.
     */
    public static boolean isValidToken(String secret, long userToken, int windowMillis) {
        return isValidToken(secret, userToken, windowMillis, System.currentTimeMillis(),
            DEFAULT_TIME_STEP_SECONDS);
    }

    /**
     * <p>Similar to {@link #isValidToken(String, long, int)} except exposes
     * other parameters.</p>
     *
     * @param secret          secret string encoded using base-32 that was used
     *                        to generate the QR code or shared with the user.
     * @param userToken       time based token provided by the user from their
     *                        authenticator application.
     * @param windowMillis    number of milliseconds that are allowed to be off
     *                        and still match. This checks before and after the
     *                        current time to account for clock variance. Set to
     *                        0 for no window.
     * @param timeMillis      time in milliseconds.
     * @param timeStepSeconds time step in seconds. default value is 30 seconds
     *                        here. See {@link #DEFAULT_TIME_STEP_SECONDS}.
     * @return true if the token matched the calculated number within the
     * specified window.
     */
    public static boolean isValidToken(String secret, long userToken,
                                       int windowMillis, long timeMillis,
                                       int timeStepSeconds) {
        long from = timeMillis;
        long to = timeMillis;
        if (windowMillis > 0) {
            from -= windowMillis;
            to += windowMillis;
        }

        long timeStepMillis = TimeUnit.SECONDS.toMillis(timeStepSeconds);
        for (long millis = from; millis <= to; millis += timeStepMillis) {
            long currentToken = generateToken(secret, millis, timeStepSeconds);
            if (currentToken == userToken) {
                return true;
            }
        }

        return false;
    }

    /**
     * <p>Generates a token leveraging the stored secret and the default timeStep
     * defined in {@link #DEFAULT_TIME_STEP_SECONDS}. Note that this token is
     * numerical and will lack the leading zeros. Front end implementations that
     * are provided by the user are casted to a long.</p>
     *
     * @param secret the stored secret for the user
     * @return the token for the current time
     */
    public static long generateToken(String secret) {
        return generateToken(secret, System.currentTimeMillis(), DEFAULT_TIME_STEP_SECONDS);
    }

    /**
     * <p>Generate a token leveraging the stored secret and allows for optional
     * the time frame in which a token is being generated for and a custom non
     * default time step. Intended use case is largely for testing.</p>
     *
     * @param secret          the secret to base our token from
     * @param timeMillis      the time frame we are building a token for
     * @param timeStepSeconds the time skip to be observed
     * @return
     */
    public static long generateToken(String secret, long timeMillis, int timeStepSeconds) {
        try {
            byte[] key = new Base32().decode(secret);

            byte[] data = new byte[8];
            long value = timeMillis / 1000 / timeStepSeconds;
            for (int i = 7; value > 0; i--) {
                data[i] = (byte) (value & 0xFF);
                value >>= 8;
            }

            // verbatim instruction set from rfc 4226 using HmacSHA1
            // Step 1. Generate HMAC
            SecretKeySpec keySpec = new SecretKeySpec(key, "HmacSHA1");
            Mac mac = Mac.getInstance("HmacSHA1");
            mac.init(keySpec);
            byte[] hash = mac.doFinal(data);

            // Step 2. perform dynamic truncation for 4-byte string
            int offset = hash[hash.length - 1] & 0xF;

            long truncatedHash = 0;
            for (int i = offset; i < offset + 4; ++i) {
                truncatedHash <<= 8;
                truncatedHash |= (hash[i] & 0xFF);
            }

            truncatedHash &= 0x7FFFFFFF;

            truncatedHash %= 1000000;

            return truncatedHash;
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /**
     * <p>Provides the key uri that should be rendered to a QR code by a front
     * end service. Because this implementation only supports TOTP the TYPE has
     * been hardcoded to totp.</p>
     *
     * @param keyId  name of the key that you want to show up in the users
     *               authentication application
     * @param secret secret string that will be used when generating tokens
     */
    public static String generateOtpAuthUrl(String keyId, String secret) {
        try {
            return String.format("otpauth://totp/%s?secret=%s",
                URLEncoder.encode(keyId, "UTF-8"), secret);
        } catch (UnsupportedEncodingException e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }
}
