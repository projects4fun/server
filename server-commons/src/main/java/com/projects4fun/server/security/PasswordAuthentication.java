package com.projects4fun.server.security;

import java.util.Base64;
import java.util.regex.Pattern;

/**
 * @author Juan Garcia
 * @since 2018-07-30
 */
public interface PasswordAuthentication {

    Base64.Encoder ENCODER = Base64.getEncoder();
    Pattern COMPONENT_PATTERN = Pattern.compile("\\$");

    /**
     * <p>Takes a password and hashes it using the algorithm, cost, and salt of
     * the current hash. After the hash is complete it is compared against the
     * previously hashed password. If the two results are equal the password is
     * considered valid.</p>
     *
     * @param password the password that is being validated
     * @param hash     the hashed password to be validated against
     * @return true if the hash results match; false otherwise
     */
    boolean isValidPw(String password, String hash);

    /**
     * <p>Permits a class to extend {@link PasswordProtectedSubject}. If a class
     * elects to do so this method can be used to hash the password of the
     * subject.</p>
     *
     * @param subject the object that contains a password
     */
    void hashPw(PasswordProtectedSubject subject);

    /**
     * <p>Takes a {@link String} and applies the implementation specific hash.</p>
     *
     * @param password the {@link String} to be hashed
     * @return the hash results
     */
    String hashPw(String password);

    /**
     * <p>Determines the class to be used for performing a hash. This method
     * relies on the internal naming scheme of 4 character tokens. For example
     * #21# and #31# are two tokens that implementations have defined..</p>
     *
     * @param hash the previously defined hash to infer a token
     * @return the implementation that may be used to validate a password
     */
    static PasswordAuthentication getInstance(String hash) {
        final String token = hash.substring(0, 4);

        switch (token) {
            case PBKDF2PasswordAuthentication.TOKEN:
                return new PBKDF2PasswordAuthentication();
            default:
                throw new IllegalArgumentException();
        }
    }
}
