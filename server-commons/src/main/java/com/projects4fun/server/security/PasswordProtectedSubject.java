package com.projects4fun.server.security;

/**
 * <p>The password protected user allows for an easier means of ensuring that
 * passwords are hashed. We do not hash the password in the getter and setter to
 * allow functions such as Jackson and Hibernate to work with these methods and
 * not end in a unknown state. When hashing a password instead call on on method
 * {@link PBKDF2PasswordAuthentication#hashPw(PasswordProtectedSubject)} which
 * hashes the users password assuming one is provided.</p>
 *
 * @author Juan Garcia
 * @since 2018-04-06
 */
public interface PasswordProtectedSubject {

    String getPassword();

    void setPassword(String password);
}
